package config

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/mysql"
	"github.com/maurodelazeri/westeros/portfolio"
	"github.com/sirupsen/logrus"
)

// Variables here are mainly alerts and a configuration object
var (
	ErrExchangeNameEmpty                       = "Exchange #%d in config: Exchange name is empty."
	ErrExchangeBaseCurrenciesEmpty             = "Exchange %s: Base currencies is empty."
	ErrExchangeNotFound                        = "Exchange %s: Not found."
	ErrNoEnabledExchanges                      = "No Exchanges enabled."
	ErrFailureOpeningConfig                    = "Fatal error opening %s file. Error: %s"
	ErrCheckingConfigValues                    = "Fatal error checking config values. Error: %s"
	ErrSavingConfigBytesMismatch               = "Config file %q bytes comparison doesn't match, read %s expected %s."
	WarningExchangeAuthAPIDefaultOrEmptyValues = "WARNING -- Exchange %s: Authenticated API support disabled due to default/empty APIKey/Secret/ClientID values."
	Cfg                                        Config
)

// Post holds the bot configuration data
type Post struct {
	Data Config `json:"Data"`
}

// Config is the overarching object that holds all the information for
// prestart management of portfolio, webserver and enabled exchange
type Config struct {
	Portfolio    portfolio.Base   `json:"PortfolioAddresses"`
	Exchanges    []ExchangeConfig `json:"Exchanges"`
	mysqlSession *sql.DB
}

// ExchangeConfig holds all the information needed for each enabled Exchange.
type ExchangeConfig struct {
	Name                    string
	Enabled                 bool
	Verbose                 bool
	Websocket               bool
	WebsocketDedicated      []string
	KafkaStreaming          bool
	PushServer              bool
	RESTPollingDelay        time.Duration
	AuthenticatedAPISupport bool
	APIKey                  string
	APISecret               string
	ClientID                string
	ExchangeEnabledPairs    []string
	APIEnabledPairs         []string
	BaseCurrencies          string
	AssetTypes              string
	Markets                 []string
	MakerFee                float64
	TakerFee                float64
}

// GetEnabledExchanges returns a list of enabled exchanges
func (c *Config) GetEnabledExchanges() []string {
	var enabledExchs []string
	for i := range c.Exchanges {
		if c.Exchanges[i].Enabled {
			enabledExchs = append(enabledExchs, c.Exchanges[i].Name)
		}
	}
	return enabledExchs
}

// GetDisabledExchanges returns a list of disabled exchanges
func (c *Config) GetDisabledExchanges() []string {
	var disabledExchs []string
	for i := range c.Exchanges {
		if !c.Exchanges[i].Enabled {
			disabledExchs = append(disabledExchs, c.Exchanges[i].Name)
		}
	}
	return disabledExchs
}

// CountEnabledExchanges returns the number of exchanges that are enabled.
func (c *Config) CountEnabledExchanges() int {
	counter := 0
	for i := range c.Exchanges {
		if c.Exchanges[i].Enabled {
			counter++
		}
	}
	return counter
}

// GetEnabledPairs returns the pairs enabled to a exchange
func (c *Config) GetEnabledPairs(exchange string) []string {
	for i := range c.Exchanges {
		if exchange == c.Exchanges[i].Name {
			return c.Exchanges[i].APIEnabledPairs
		}
	}
	return nil
}

// GetEnabledPairsNornalized returns the pairs enabled to a exchange
func (c *Config) GetEnabledPairsNornalized(exchange string) []string {
	for i := range c.Exchanges {
		if exchange == c.Exchanges[i].Name {
			return c.Exchanges[i].ExchangeEnabledPairs
		}
	}
	return nil
}

// GetSocketUsage returns the exchange is using socket or not
func (c *Config) GetSocketUsage(exchange string) bool {
	for i := range c.Exchanges {
		if exchange == c.Exchanges[i].Name {
			return c.Exchanges[i].Websocket
		}
	}
	return false
}

// GetExchangeConfig returns your exchange configurations by its indivdual name
func (c *Config) GetExchangeConfig(name string) (ExchangeConfig, error) {
	for i := range c.Exchanges {
		if c.Exchanges[i].Name == name {
			return c.Exchanges[i], nil
		}
	}
	return ExchangeConfig{}, fmt.Errorf(ErrExchangeNotFound, name)
}

// CheckExchangeConfigValues returns configuation values for all enabled
// exchanges
func (c *Config) CheckExchangeConfigValues() error {
	c.mysqlSession = mysqlserver.GetMysqlSession()
	//sql := "SELECT e.name,e.enabled,e.verbose,e.websocket,e.influx_streaming,e.kafka_streaming,e.authenticated_api_support,e.api_key,e.api_secret,e.clientID,e.passphrase,e.basecurrencies,e.assettypes,e.markets,e.taker,e. maker, (SELECT GROUP_CONCAT(pair) FROM exchange_pair p WHERE e.id=p.exchange_id and p.enabled=1) as enabledPairs,(SELECT GROUP_CONCAT(pair_exchange) FROM exchange_pair p WHERE e.id=p.exchange_id and p.enabled=1) as exchangePairs FROM exchange e, exchange_pair p WHERE e.enabled=1 group by e.id"
	sql := "SELECT e.name,e.enabled,e.verbose,e.websocket,e.kafka_streaming,e.push_server,e.authenticated_api_support,e.api_key,e.api_secret,e.clientID,e.passphrase,e.basecurrencies,e.assettypes,e.markets,e.taker,e. maker, COALESCE((SELECT GROUP_CONCAT(pair) FROM exchange_pair p WHERE e.id=p.exchange_id and p.enabled=1),'0') as enabledPairs, COALESCE((SELECT GROUP_CONCAT(pair_exchange) FROM exchange_pair p WHERE e.id=p.exchange_id and p.enabled=1),'0') as exchangePairs, COALESCE((SELECT GROUP_CONCAT(pair_exchange) FROM exchange_pair p WHERE e.id=p.exchange_id and p.enabled=1 and p.websocket_dedicated=1),'0') as websocketDedicated FROM exchange e, exchange_pair p group by e.id"
	rows, err := c.mysqlSession.Query(sql)

	checkErr(err)
	//websocket_dedicated
	for rows.Next() {
		var name string
		var verbose bool
		var enabled bool
		var websocket bool
		var kafkaStreaming bool
		var pushServer bool
		var authenticatedAPISupport bool
		var apiKey string
		var apiSecret string
		var clientID string
		var passphrase string
		var basecurrencies string
		var assettypes string
		var markets string
		var taker float64
		var maker float64

		var enabledPairs string
		var exchangePairs string
		var websocketDedicated string

		err = rows.Scan(&name, &enabled, &verbose, &websocket, &kafkaStreaming, &pushServer, &authenticatedAPISupport, &apiKey, &apiSecret, &clientID, &passphrase, &basecurrencies, &assettypes, &markets, &taker, &maker, &enabledPairs, &exchangePairs, &websocketDedicated)
		checkErr(err)

		if enabledPairs == "" || exchangePairs == "" {
			logrus.Error("Exchange ", name, " pairs are not properly configured")
			continue
		}

		var exchange ExchangeConfig

		if enabledPairs != "0" || exchangePairs != "0" {
			exchange.APIEnabledPairs = common.SplitStrings(strings.Replace(enabledPairs, " ", "", -1), ",")
			exchange.ExchangeEnabledPairs = common.SplitStrings(strings.Replace(exchangePairs, " ", "", -1), ",")
		}

		if websocketDedicated == "0" {
			exchange.WebsocketDedicated = []string{}
		} else {
			exchange.WebsocketDedicated = common.SplitStrings(strings.Replace(websocketDedicated, " ", "", -1), ",")
		}

		exchange.Name = name
		exchange.Enabled = enabled
		exchange.Verbose = verbose
		exchange.Websocket = websocket
		exchange.KafkaStreaming = kafkaStreaming
		exchange.PushServer = pushServer
		exchange.RESTPollingDelay = 10
		exchange.AuthenticatedAPISupport = authenticatedAPISupport
		exchange.APIKey = apiKey
		exchange.APISecret = apiSecret
		exchange.ClientID = clientID

		exchange.BaseCurrencies = basecurrencies
		exchange.AssetTypes = assettypes
		exchange.MakerFee = maker
		exchange.TakerFee = taker
		if enabled {
			logrus.Info("EXCHANGE: ", name, " ENABLED ", len(exchange.APIEnabledPairs), " pairs")
		}

		c.Exchanges = append(c.Exchanges, exchange)
	}

	if len(c.Exchanges) == 0 {
		logrus.Info("There is no exchanges enabled")
	}

	exchanges := 0
	for i, exch := range c.Exchanges {
		if exch.Enabled {
			if exch.Name == "" {
				return fmt.Errorf(ErrExchangeNameEmpty, i)
			}
			if exch.BaseCurrencies == "" {
				return fmt.Errorf(ErrExchangeBaseCurrenciesEmpty, exch.Name)
			}
			if exch.AuthenticatedAPISupport { // non-fatal error
				if exch.APIKey == "" || exch.APISecret == "" || exch.APIKey == "Key" || exch.APISecret == "Secret" {
					c.Exchanges[i].AuthenticatedAPISupport = false
					log.Printf(WarningExchangeAuthAPIDefaultOrEmptyValues, exch.Name)
				} else if exch.Name == "Bitstamp" || exch.Name == "GDAX" {
					if exch.ClientID == "" || exch.ClientID == "ClientID" {
						c.Exchanges[i].AuthenticatedAPISupport = false
						log.Printf(WarningExchangeAuthAPIDefaultOrEmptyValues, exch.Name)
					}
				}
			}
			exchanges++
		}
	}
	if exchanges == 0 {
		return errors.New(ErrNoEnabledExchanges)
	}
	return nil
}

// LoadConfig loads your configuration file into your configuration object
func (c *Config) LoadConfig() error {
	err := c.CheckExchangeConfigValues()
	if err != nil {
		return fmt.Errorf(ErrCheckingConfigValues, err)
	}
	return nil
}

// GetConfig returns a pointer to a confiuration object
func GetConfig() *Config {
	return &Cfg
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
