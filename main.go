package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/config"
	"github.com/maurodelazeri/westeros/exchanges"
	"github.com/maurodelazeri/westeros/exchanges/binance"
	"github.com/maurodelazeri/westeros/exchanges/bitfinex"
	"github.com/maurodelazeri/westeros/exchanges/bitmex"
	"github.com/maurodelazeri/westeros/exchanges/gdax"
	"github.com/maurodelazeri/westeros/portfolio"
	"github.com/sirupsen/logrus"
)

// Westeros contains configuration, portfolio, exchange & ticker data and is the
// overarching type across this code base.
type Westeros struct {
	config     *config.Config
	portfolio  *portfolio.Base
	arbitrage  exchange.Base
	exchanges  []exchange.IWesterosExchange
	shutdown   chan bool
	configFile string
}

// vars related to exchange functions
var (
	ErrNoExchangesLoaded     = errors.New("no exchanges have been loaded")
	ErrExchangeNotFound      = errors.New("exchange not found")
	ErrExchangeAlreadyLoaded = errors.New("exchange already loaded")
	ErrExchangeFailedToLoad  = errors.New("exchange failed to load")
)

const banner = `

██╗    ██╗███████╗███████╗████████╗███████╗██████╗  ██████╗ ███████╗
██║    ██║██╔════╝██╔════╝╚══██╔══╝██╔════╝██╔══██╗██╔═══██╗██╔════╝
██║ █╗ ██║█████╗  ███████╗   ██║   █████╗  ██████╔╝██║   ██║███████╗
██║███╗██║██╔══╝  ╚════██║   ██║   ██╔══╝  ██╔══██╗██║   ██║╚════██║
╚███╔███╔╝███████╗███████║   ██║   ███████╗██║  ██║╚██████╔╝███████║
 ╚══╝╚══╝ ╚══════╝╚══════╝   ╚═╝   ╚══════╝╚═╝  ╚═╝ ╚═════╝ ╚══════╝ 
`

var westeros Westeros

func main() {
	//https://github.com/alioygur/wait-for
	sleep := os.Getenv("SLEEP")
	if sleep == "true" {
		logrus.Info("Sleeping 15s to let all services be up...")
		time.Sleep(15 * time.Second)
	}

	HandleInterrupt()

	westeros.config = &config.Cfg
	fmt.Println(banner)
	logrus.Infof("Loading config...")

	err := westeros.config.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	AdjustGoMaxProcs()

	logrus.Infof(
		"Available Exchanges: %d. Enabled Exchanges: %d.\n",
		len(westeros.config.Exchanges), westeros.config.CountEnabledExchanges(),
	)

	SetupExchanges()

	if len(westeros.exchanges) == 0 {
		log.Fatalf("No exchanges were able to be loaded. Exiting")
		os.Exit(0)
	}

	mode := os.Getenv("MODE")
	switch mode {
	case "push":
		logrus.Info("Push mode")
	case "streaming":
		logrus.Info("Streaming mode")
	case "execution":
		logrus.Info("Execution mode")
		// Portifolio handles
		westeros.portfolio = &portfolio.Portfolio
		westeros.portfolio.SeedPortfolio(westeros.config.Portfolio)
		go portfolio.StartPortfolioWatcher()
	default:
		logrus.Warn("Please specify a mode to start an exchange.")
		os.Exit(1)
	}

	logrus.Infof("Westeros Started.\n")

	<-westeros.shutdown
	Shutdown()
}

// AdjustGoMaxProcs adjusts the maximum processes that the CPU can handle.
func AdjustGoMaxProcs() {
	logrus.Info("Adjusting westeros runtime performance..")
	maxProcsEnv := os.Getenv("GOMAXPROCS")
	maxProcs := runtime.NumCPU()
	logrus.Info("Number of CPU's detected:", maxProcs)

	if maxProcsEnv != "" {
		logrus.Info("GOMAXPROCS env =", maxProcsEnv)
		env, err := strconv.Atoi(maxProcsEnv)
		if err != nil {
			logrus.Info("Unable to convert GOMAXPROCS to int, using", maxProcs)
		} else {
			maxProcs = env
		}
	}
	if i := runtime.GOMAXPROCS(maxProcs); i != maxProcs {
		log.Fatal("Go Max Procs were not set correctly.")
	}
	logrus.Info("Set GOMAXPROCS to:", maxProcs)
}

// HandleInterrupt monitors and captures the SIGTERM in a new goroutine then
// shuts down westeros
func HandleInterrupt() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		sig := <-c
		logrus.Infof("Captured %v.", sig)
		Shutdown()
	}()
}

// Shutdown correctly shuts down westeros saving configuration files
func Shutdown() {
	logrus.Info("Westeros shutting down..")
	os.Exit(1)
}

// SeedExchangeAccountInfo seeds account info
func SeedExchangeAccountInfo(data []exchange.AccountInfo) {
	if len(data) == 0 {
		return
	}

	port := portfolio.GetPortfolio()

	for i := 0; i < len(data); i++ {
		exchangeName := data[i].ExchangeName
		for j := 0; j < len(data[i].Currencies); j++ {
			currencyName := data[i].Currencies[j].CurrencyName
			onHold := data[i].Currencies[j].Hold
			avail := data[i].Currencies[j].TotalValue
			total := onHold + avail

			if !port.ExchangeAddressExists(exchangeName, currencyName) {
				if total <= 0 {
					continue
				}
				logrus.Infof("Portfolio: Adding new exchange address: %s, %s, %f, %s\n",
					exchangeName, currencyName, total, portfolio.PortfolioAddressExchange)
				port.Addresses = append(
					port.Addresses,
					portfolio.Address{Address: exchangeName, CoinType: currencyName,
						Balance: total, Description: portfolio.PortfolioAddressExchange},
				)
			} else {
				if total <= 0 {
					logrus.Infof("Portfolio: Removing %s %s entry.\n", exchangeName,
						currencyName)
					port.RemoveExchangeAddress(exchangeName, currencyName)
				} else {
					balance, ok := port.GetAddressBalance(exchangeName, currencyName, portfolio.PortfolioAddressExchange)
					if !ok {
						continue
					}
					if balance != total {
						logrus.Infof("Portfolio: Updating %s %s entry with balance %f.\n",
							exchangeName, currencyName, total)
						port.UpdateExchangeAddressBalance(exchangeName, currencyName, total)
					}
				}
			}
		}
	}
}

// CheckExchangeExists returns true whether or not an exchange has already
// been loaded
func CheckExchangeExists(exchName string) bool {
	for x := range westeros.exchanges {
		if common.StringToLower(westeros.exchanges[x].GetName()) == common.StringToLower(exchName) {
			return true
		}
	}
	return false
}

// GetExchangeByName returns an exchange given an exchange name
func GetExchangeByName(exchName string) exchange.IWesterosExchange {
	for x := range westeros.exchanges {
		if common.StringToLower(westeros.exchanges[x].GetName()) == common.StringToLower(exchName) {
			return westeros.exchanges[x]
		}
	}
	return nil
}

// ReloadExchange loads an exchange config by name
func ReloadExchange(name string) error {
	nameLower := common.StringToLower(name)

	if len(westeros.exchanges) == 0 {
		return ErrNoExchangesLoaded
	}

	if !CheckExchangeExists(nameLower) {
		return ErrExchangeNotFound
	}

	exchCfg, err := westeros.config.GetExchangeConfig(name)
	if err != nil {
		return err
	}

	e := GetExchangeByName(nameLower)
	e.Setup(exchCfg)
	log.Printf("%s exchange reloaded successfully.\n", name)
	return nil
}

// UnloadExchange unloads an exchange by
func UnloadExchange(name string) error {
	nameLower := common.StringToLower(name)

	if len(westeros.exchanges) == 0 {
		return ErrNoExchangesLoaded
	}

	if !CheckExchangeExists(nameLower) {
		return ErrExchangeNotFound
	}

	for x := range westeros.exchanges {
		if westeros.exchanges[x].GetName() == name {
			westeros.exchanges[x].SetEnabled(false)
			westeros.exchanges = append(westeros.exchanges[:x], westeros.exchanges[x+1:]...)
			return nil
		}
	}

	return ErrExchangeNotFound
}

// LoadExchange loads an exchange by name
func LoadExchange(name string) error {
	nameLower := common.StringToLower(name)
	var exch exchange.IWesterosExchange

	if len(westeros.exchanges) > 0 {
		if CheckExchangeExists(nameLower) {
			return ErrExchangeAlreadyLoaded
		}
	}

	switch nameLower {
	case "bitfinex":
		exch = new(bitfinex.Bitfinex)
	case "bitmex":
		exch = new(bitmex.Bitmex)
	case "binance":
		exch = new(binance.Binance)
	case "gdax":
		exch = new(gdax.GDAX)
	// case "gemini":
	// 	exch = new(gemini.Gemini)

	default:
		return ErrExchangeNotFound
	}

	if exch == nil {
		return ErrExchangeFailedToLoad
	}

	exch.SetDefaults()
	westeros.exchanges = append(westeros.exchanges, exch)
	exchCfg, err := westeros.config.GetExchangeConfig(name)
	if err != nil {
		return err
	}

	exchCfg.Enabled = true
	exch.Setup(exchCfg)
	exch.Start()

	return nil
}

// SetupExchanges sets up the exchanges used by the westeros
func SetupExchanges() {

	for _, exch := range westeros.config.Exchanges {
		if CheckExchangeExists(exch.Name) {
			e := GetExchangeByName(exch.Name)
			if e == nil {
				log.Println(ErrExchangeNotFound)
				continue
			}

			err := ReloadExchange(exch.Name)
			if err != nil {
				log.Printf("ReloadExchange %s failed: %s", exch.Name, err)
				continue
			}

			if !e.IsEnabled() {
				UnloadExchange(exch.Name)
				continue
			}
			return

		}
		if !exch.Enabled {
			log.Printf("%s: Exchange support: Disabled", exch.Name)
			continue
		} else {
			err := LoadExchange(exch.Name)
			if err != nil {
				log.Printf("LoadExchange %s failed: %s", exch.Name, err)
				continue
			}
		}

		log.Printf(
			"%s: Exchange support: Enabled (Authenticated API support: %s - Verbose mode: %s).\n",
			exch.Name,
			common.IsEnabled(exch.AuthenticatedAPISupport),
			common.IsEnabled(exch.Verbose),
		)
	}
}

/*
docker build -t westeros . && docker run -it westeros
docker-compose up -d --build

export REDIS=127.0.0.1:6379
export DB_HOST=127.0.0.1:3306

export MYSQLUSER=root
export MYSQLPASS=123456
export BROKERS="127.0.0.1:9092"
export TELEGRAMAPI="552268599:AAHBd9PEyiQ5XB_o5DNBU8gp_kRArXDt8ms"
export TELEGRAMGROUP="-265140096"
export SLEEP="false"
export PUSH_GDAX_BIND="tcp://:2020"
export PUSH_BITFINEX_BIND="tcp://:2021"
export PUSH_BINANCE_BIND="tcp://:2022"
export PUSH_BITMEX_BIND="tcp://:2023"
export MODE="push"
export MODE="streaming"

export REDIS=192.168.3.100:6379
export DB_HOST=192.168.3.100:3306

*/
