package redisserver

import (
	"net"
	"os"
	"time"

	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
)

// RedisServer stores the main session
type RedisServer struct {
	RedisClient *redis.Client
}

// Initialize a redis instance
func (r *RedisServer) Initialize() {
	redisconn := os.Getenv("REDIS")

	for {
		conn, err := net.DialTimeout("tcp", redisconn, 6*time.Second)
		if conn != nil {
			conn.Close()
			break
		}

		logrus.Info("Sleeping till redis be available... ", err)
		time.Sleep(5 * time.Second)
	}

	client := redis.NewClient(&redis.Options{
		Addr:     redisconn,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	_, err := client.Ping().Result()
	if err != nil {
		logrus.Info("No connection with redis, ", err)
		os.Exit(1)
	}

	r.RedisClient = client
}
