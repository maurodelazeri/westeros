package kafkaserver

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/maurodelazeri/westeros/common"

	"github.com/maurodelazeri/westeros/exchanges/orderbook"
	"github.com/maurodelazeri/westeros/exchanges/ticker"
	"github.com/sirupsen/logrus"
)

// KafkaConsumer listem to topics on kafka
func KafkaConsumer() {
	go func() {

		broker := os.Getenv("BROKERS")
		group := os.Getenv("KAFKAGROUP")
		topics := os.Getenv("KAFKATOPICS")

		c, err := kafka.NewConsumer(&kafka.ConfigMap{
			"bootstrap.servers":               broker,
			"group.id":                        group,
			"session.timeout.ms":              6000,
			"go.events.channel.enable":        true,
			"go.application.rebalance.enable": true,
			"default.topic.config":            kafka.ConfigMap{"auto.offset.reset": "earliest"}})

		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to create consumer: %s\n", err)
			os.Exit(1)
		}

		fmt.Printf("Created Consumer %v\n", c)

		err = c.SubscribeTopics(common.SplitStrings(topics, ","), nil)

		for {
			select {
			case ev := <-c.Events():
				switch e := ev.(type) {
				case kafka.AssignedPartitions:
					fmt.Fprintf(os.Stderr, "%% %v\n", e)
					c.Assign(e.Partitions)
				case kafka.RevokedPartitions:
					fmt.Fprintf(os.Stderr, "%% %v\n", e)
					c.Unassign()
				case *kafka.Message:
					switch *e.TopicPartition.Topic {
					case "trades":
						trade := ticker.Trade{}
						err := json.Unmarshal(e.Value, &trade)
						if err != nil {
							logrus.Println(err)
							continue
						}

					//	influxserver.MetricsPointCh <- influxserver.InfluxTrades(trade)

					case "tick":
						tick := ticker.Price{}
						err := json.Unmarshal(e.Value, &tick)
						if err != nil {
							logrus.Println(err)
							continue
						}

					//	influxserver.MetricsPointCh <- influxserver.InfluxTicker(tick)

					case "orderbook":
						orderbook := orderbook.Base{}
						err := json.Unmarshal(e.Value, &orderbook)
						if err != nil {
							logrus.Println(err)
							continue
						}
						if len(orderbook.Asks) > 9 && len(orderbook.Bids) > 9 {
							//	influxserver.MetricsPointCh <- influxserver.InfluxOrderbook(orderbook)
						}

					}
				case kafka.PartitionEOF:
					// fmt.Printf("%% Reached %v\n", e)
				case kafka.Error:
					fmt.Fprintf(os.Stderr, "%% Error: %v\n", e)
				}
			}
		}
	}()
}
