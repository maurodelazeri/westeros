package kafkaserver

import (
	"fmt"
	"os"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/sirupsen/logrus"
)

// KafkaProducer stores the main session
type KafkaProducer struct {
	Kafkalient *kafka.Producer
}

// Initialize a kafka instance
func (k *KafkaProducer) Initialize() {
	brokers := os.Getenv("BROKERS")
	client, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": brokers, "message.max.bytes": 100857600})
	if err != nil {
		logrus.Info("No connection with kafka, ", err)
		os.Exit(1)
	}
	k.Kafkalient = client
}

// StreamingMessage send a message to kafka server
func (k *KafkaProducer) StreamingMessage(topic string, value string, verbose bool) {
	p := k.Kafkalient
	deliveryChan := make(chan kafka.Event, 10000)
	err := p.Produce(&kafka.Message{TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny}, Value: []byte(value)}, deliveryChan)

	if err != nil {
		logrus.Error("Kafka Produce streamingSinglePipeline", err)
	}

	e := <-deliveryChan
	m := e.(*kafka.Message)

	if verbose {
		if m.TopicPartition.Error != nil {
			fmt.Printf("Delivery failed: %v\n", m.TopicPartition.Error)
		} else {
			fmt.Printf("Delivered message to topic %s [%d] at offset %v\n",
				*m.TopicPartition.Topic, m.TopicPartition.Partition, m.TopicPartition.Offset)
		}
	}

	close(deliveryChan)
}
