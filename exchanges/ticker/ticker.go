package ticker

import (
	"strconv"
	"time"

	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/currency/pair"
)

// Const values for the ticker package
const (
	ErrTickerForExchangeNotFound = "Ticker for exchange does not exist."
	ErrPrimaryCurrencyNotFound   = "Error primary currency for ticker not found."
	ErrSecondaryCurrencyNotFound = "Error secondary currency for ticker not found."

	Spot = "SPOT"
)

// Vars for the ticker package
var (
	Tickers []Ticker
)

// Price struct stores the currency pair and pricing information
type Price struct {
	Exchange          string            `json:"Exchange"`
	Pair              pair.CurrencyPair `json:"Pair"`
	CurrencyPair      string            `json:"CurrencyPair"`
	Last              float64           `json:"Last"`
	High              float64           `json:"High"`
	Low               float64           `json:"Low"`
	Bid               float64           `json:"Bid"`
	Ask               float64           `json:"Ask"`
	Volume            float64           `json:"Volume"`
	UpdateTime        time.Time         `json:"UpdateTime"`
	WeekDayNormalized string            `json:"WeekDayNormalized"`
	WeekDay           int               `json:"WeekDay"`
	PriceATH          float64           `json:"PriceATH"`
}

// Trade stores trades
type Trade struct {
	Exchange          string            `json:"Exchange"`
	Pair              pair.CurrencyPair `json:"Pair"`
	CurrencyPair      string            `json:"CurrencyPair"`
	Price             float64           `json:"Price"`
	Size              float64           `json:"Size"`
	Side              string            `json:"Side"`
	UpdateTime        time.Time         `json:"UpdateTime"`
	WeekDayNormalized string            `json:"WeekDayNormalized"`
	WeekDay           int               `json:"WeekDay"`
}

// Ticker struct holds the ticker information for a currency pair and type
type Ticker struct {
	Price        map[pair.CurrencyItem]map[pair.CurrencyItem]map[string]Price
	ExchangeName string
}

// PriceToString returns the string version of a stored price field
func (t *Ticker) PriceToString(p pair.CurrencyPair, priceType, tickerType string) string {
	priceType = common.StringToLower(priceType)

	switch priceType {
	case "last":
		return strconv.FormatFloat(t.Price[p.FirstCurrency][p.SecondCurrency][tickerType].Last, 'f', -1, 64)
	case "high":
		return strconv.FormatFloat(t.Price[p.FirstCurrency][p.SecondCurrency][tickerType].High, 'f', -1, 64)
	case "low":
		return strconv.FormatFloat(t.Price[p.FirstCurrency][p.SecondCurrency][tickerType].Low, 'f', -1, 64)
	case "bid":
		return strconv.FormatFloat(t.Price[p.FirstCurrency][p.SecondCurrency][tickerType].Bid, 'f', -1, 64)
	case "ask":
		return strconv.FormatFloat(t.Price[p.FirstCurrency][p.SecondCurrency][tickerType].Ask, 'f', -1, 64)
	case "volume":
		return strconv.FormatFloat(t.Price[p.FirstCurrency][p.SecondCurrency][tickerType].Volume, 'f', -1, 64)
	case "ath":
		return strconv.FormatFloat(t.Price[p.FirstCurrency][p.SecondCurrency][tickerType].PriceATH, 'f', -1, 64)
	default:
		return ""
	}
}

// FirstCurrencyExists checks to see if the first currency of the Price map
// exists
func FirstCurrencyExists(exchange string, currency pair.CurrencyItem) bool {
	for _, y := range Tickers {
		if y.ExchangeName == exchange {
			if _, ok := y.Price[currency]; ok {
				return true
			}
		}
	}
	return false
}

// SecondCurrencyExists checks to see if the second currency of the Price map
// exists
func SecondCurrencyExists(exchange string, p pair.CurrencyPair) bool {
	for _, y := range Tickers {
		if y.ExchangeName == exchange {
			if _, ok := y.Price[p.GetFirstCurrency()]; ok {
				if _, ok := y.Price[p.GetFirstCurrency()][p.GetSecondCurrency()]; ok {
					return true
				}
			}
		}
	}
	return false
}
