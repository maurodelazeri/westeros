package binance

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/jpillora/backoff"
	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/compress"
	"github.com/maurodelazeri/westeros/exchanges/orderbook"
	"github.com/sirupsen/logrus"
)

// Close closes the underlying network connection without
// sending or waiting for a close frame.
func (b *WebsocketBinance) Close() {
	b.mu.Lock()
	if b.Conn != nil {
		b.Conn.Close()
	}
	b.isConnected = false
	b.mu.Unlock()
}

func (b *WebsocketBinance) connect() {

	bb := &backoff.Backoff{
		Min:    b.RecIntvlMin,
		Max:    b.RecIntvlMax,
		Factor: b.RecIntvlFactor,
		Jitter: true,
	}

	rand.Seed(time.Now().UTC().UnixNano())

	b.OrderBookMAP = make(map[string]map[float64]float64)
	b.OrderBookBasicStats = make(map[string]*orderbook.BookStats)
	b.LiveOrderBook = make(map[string]*orderbook.Base)
	b.LockTillBookFetchToFinish = map[string]int64{}
	b.NextBookID = map[string]int64{}

	for _, sym := range b.subscribedPairs {

		position, exist := b.base.StringInSlice(sym, b.base.ExchangeEnabledPairs)
		symbol := strings.Split(b.base.APIEnabledPairs[position], "/")
		composedSymbol := symbol[0] + symbol[1]
		if exist {

			b.OrderBookBasicStats[composedSymbol] = &orderbook.BookStats{}

			b.OrderBookMAP[composedSymbol+"bids"] = make(map[float64]float64)
			b.OrderBookMAP[composedSymbol+"asks"] = make(map[float64]float64)

			// err := b.base.RedisServer.RedisClient.Del("stats" + composedSymbol + b.base.Name).Err()
			// if err != nil {
			// 	logrus.Error(err)
			// }
		}
	}

	for {
		nextItvl := bb.Duration()

		wsConn, httpResp, err := b.dialer.Dial(b.url, b.reqHeader)

		b.mu.Lock()
		b.Conn = wsConn
		b.dialErr = err
		b.isConnected = err == nil
		b.httpResp = httpResp
		b.mu.Unlock()

		if err == nil {
			if b.Verbose {
				logrus.Printf("Dial: connection was successfully established with %s\n", b.url)
			}
			break
		} else {
			if b.Verbose {
				logrus.Println(err)
				logrus.Println("Dial: will try again in", nextItvl, "seconds.")
			}
		}

		time.Sleep(nextItvl)
	}
}

// FetchEnabledOrderBooks gets the initial orderbook
func (b *WebsocketBinance) FetchEnabledOrderBooks() {
	go func() {
		for _, p := range b.subscribedPairs {
			orderbookNew, lastUpdateID, err := b.base.GetOrderBook(p, 50)
			position, exist := b.StringInSlice(p, b.base.ExchangeEnabledPairs)
			if err != nil {
				logrus.Error(b.base.Name, " ", p, " Problem to Fetch orderbook ", err)
				time.Sleep(1 * time.Second)
				continue
			}
			if exist {
				symbolChannel := strings.Split(b.base.APIEnabledPairs[position], "/")
				composedSymbol := symbolChannel[0] + symbolChannel[1]
				var wg sync.WaitGroup

				wg.Add(1)
				go func() {
					for _, bids := range orderbookNew.Bids {
						b.OrderBookMAP[composedSymbol+"bids"][bids.Price] = bids.Quantity
					}
					wg.Done()
				}()

				wg.Add(1)
				go func() {
					for _, asks := range orderbookNew.Asks {
						b.OrderBookMAP[composedSymbol+"asks"][asks.Price] = asks.Quantity
					}
					wg.Done()
				}()

				wg.Wait()

				b.LockTillBookFetchToFinish[strings.ToLower(p)+"@depth"] = lastUpdateID
				b.NextBookID[strings.ToLower(p)+"@depth"] = 0
			}
		}
	}()
}

func (b *WebsocketBinance) WebsocketClient() {

	currencies := []string{}
	for _, x := range b.subscribedPairs {
		currencies = append(currencies, strings.ToLower(x)+"@trade")
		currencies = append(currencies, strings.ToLower(x)+"@depth")
	}

	// We need a initial fetch in all books
	b.FetchEnabledOrderBooks()
	b.url = "wss://stream2.binance.com:9443/stream?streams=" + strings.Join(currencies, "/")

	if b.RecIntvlMin == 0 {
		b.RecIntvlMin = 2 * time.Second
	}

	if b.RecIntvlMax == 0 {
		b.RecIntvlMax = 30 * time.Second
	}

	if b.RecIntvlFactor == 0 {
		b.RecIntvlFactor = 1.5
	}

	if b.HandshakeTimeout == 0 {
		b.HandshakeTimeout = 2 * time.Second
	}

	b.dialer = websocket.DefaultDialer
	b.dialer.HandshakeTimeout = b.HandshakeTimeout

	// Start reading from the socket
	b.startReading()

	go func() {
		b.connect()
	}()

	// wait on first attempt
	time.Sleep(b.HandshakeTimeout)

}

// IsConnected returns the WebSocket connection state
func (b *WebsocketBinance) IsConnected() bool {
	b.mu.Lock()
	defer b.mu.Unlock()

	return b.isConnected
}

// CloseAndRecconect will try to reconnect.
func (b *WebsocketBinance) closeAndRecconect() {
	b.Close()
	go func() {
		b.connect()
	}()
}

// GetHTTPResponse returns the http response from the handshake.
// Useful when WebSocket handshake fails,
// so that callers can handle redirects, authentication, etc.
func (b *WebsocketBinance) GetHTTPResponse() *http.Response {
	b.mu.Lock()
	defer b.mu.Unlock()

	return b.httpResp
}

// GetDialError returns the last dialer error.
// nil on successful connection.
func (b *WebsocketBinance) GetDialError() error {
	b.mu.Lock()
	defer b.mu.Unlock()

	return b.dialErr
}

// ReadMessage is a helper method for getting a reader
// using NextReader and reading from that reader to a buffer.
//
// If the connection is closed ErrNotConnected is returned
// ReadMessage initiates a websocket client
func (b *WebsocketBinance) startReading() {
	go func() {
		for {
			select {
			default:
				if b.IsConnected() {
					err := ErrNotConnected
					msgType, resp, err := b.Conn.ReadMessage()
					if err != nil {
						b.closeAndRecconect()
						time.Sleep(5 * time.Second)
						break
					}
					switch msgType {
					case websocket.TextMessage:

						msgType := BinanceType{}
						err := json.Unmarshal(resp, &msgType)
						if err != nil {
							logrus.Println(err)
							continue
						}
						if strings.Contains(msgType.Stream, "@trade") {
							data := Trade{}
							err := common.JSONDecode(resp, &data)
							if err != nil {
								log.Println(err)
								continue
							}
							position, exist := b.StringInSlice(data.Data.S, b.base.ExchangeEnabledPairs)
							if exist {
								symbolChannel := strings.Split(b.base.APIEnabledPairs[position], "/")

								composedSymbol := symbolChannel[0] + symbolChannel[1]
								statsMemomory := &b.OrderBookBasicStats
								refMap := (*statsMemomory)[composedSymbol]

								var side string

								if data.Data.M {
									side = "sell"
								} else {
									side = "buy"
								}

								price, _ := strconv.ParseFloat(data.Data.P, 64)
								size, _ := strconv.ParseFloat(data.Data.Q, 64)

								if refMap.LastDealPrice > 0 {

									if refMap.LastDealPrice == price {
										refMap.TotalTradesBeforePriceImprovement++
										refMap.TotalSizeBeforePriceImprovement = refMap.TotalSizeBeforePriceImprovement + size
										refMap.Improvement = false
									} else {
										refMap.TotalSizeBeforePriceImprovement = size
										refMap.TotalTradesBeforePriceImprovement = 1
										refMap.Improvement = true

										refMap.PercentagePriceImprovement = b.base.PercentChange(refMap.LastDealPrice, price)

										refMap.WaitingTimeTillPriceImprovement = time.Since(time.Unix(0, refMap.LastDealTimestamp)).Nanoseconds()
									}

									refMap.LastDealPrice = price
									refMap.LastDealAmount = size
									refMap.LastDealSide = side
									refMap.LastDealTimestamp = b.base.MakeTimestamp()
									data.HiveTable = strings.TrimSpace(b.base.Name + "_trade_" + composedSymbol)

									(*statsMemomory)[composedSymbol] = refMap

									serialized, err := json.Marshal(data)
									if err != nil {
										logrus.Error(b.base.Name+" ", err)
										continue
									}

									if os.Getenv("MODE") == "push" {
										if b.base.PushServer {
											if err = b.base.NanoMSG.Send(append([]byte(composedSymbol+"-"+b.base.Name+"-trade"), serialized...)); err != nil {
												logrus.Error("Failed publishing: ", err.Error())
											}
										}
									} else {
										if b.base.KafkaStreaming {
											b.base.KafkaProducer.StreamingMessage(b.base.Name+"_trade_"+composedSymbol, string(serialized), b.base.Verbose)
										}
									}

								} else {
									refMap.LastDealPrice = price
									refMap.LastDealAmount = size
									refMap.LastDealSide = side
									refMap.LastDealTimestamp = b.base.MakeTimestamp()

									if side == "buy" {
										refMap.SideImprovment = 1
									} else {
										refMap.SideImprovment = 0
									}
									(*statsMemomory)[composedSymbol] = refMap
								}
							}
							continue
						}

						var depthUpdate ExOrderbook
						if err := json.Unmarshal(resp, &depthUpdate); err != nil {
							log.Println("PacketDepthUpdate-parse:", err)
							continue
						}
						if lastUpdateID, ok := b.LockTillBookFetchToFinish[strings.ToLower(depthUpdate.Data.S)+"@depth"]; ok {
							if depthUpdate.Data.U <= lastUpdateID {
								continue
							}

							b.NextBookID[strings.ToLower(depthUpdate.Data.S)+"@depth"] = lastUpdateID

							if depthUpdate.Data.UU <= lastUpdateID+1 && depthUpdate.Data.U >= lastUpdateID+1 {

								b.LockTillBookFetchToFinish[strings.ToLower(depthUpdate.Data.S)+"@depth"] = depthUpdate.Data.U + 1

								position, exist := b.StringInSlice(depthUpdate.Data.S, b.base.ExchangeEnabledPairs)
								if exist {
									symbolChannel := strings.Split(b.base.APIEnabledPairs[position], "/")
									composedSymbol := symbolChannel[0] + symbolChannel[1]
									statsMemomory := &b.OrderBookBasicStats
									refMap := (*statsMemomory)[composedSymbol]

									liveBookMemomory := &b.LiveOrderBook
									refLiveBook := (*liveBookMemomory)[composedSymbol]
									refLiveBook = &orderbook.Base{}
									refLiveBook.LastUpdate = b.base.MakeTimestamp()
									refLiveBook.WeekDay = int(time.Now().Weekday())
									refLiveBook.CurrencyPair = composedSymbol
									refLiveBook.Exchange = b.base.Name
									start := time.Now()

									for _, book := range depthUpdate.Data.Asks {
										amount, _ := strconv.ParseFloat(book[1].(string), 64)
										if amount == 0 {
											price, _ := strconv.ParseFloat(book[0].(string), 64)
											if _, ok := b.OrderBookMAP[composedSymbol+"asks"][price]; ok {
												delete(b.OrderBookMAP[composedSymbol+"asks"], price)
											}
										} else {
											price, _ := strconv.ParseFloat(book[0].(string), 64)
											amount, _ := strconv.ParseFloat(book[1].(string), 64)
											b.OrderBookMAP[composedSymbol+"asks"][price] = amount
										}
									}
									for _, book := range depthUpdate.Data.Bids {
										amount, _ := strconv.ParseFloat(book[1].(string), 64)
										if amount == 0 {
											price, _ := strconv.ParseFloat(book[0].(string), 64)
											if _, ok := b.OrderBookMAP[composedSymbol+"bids"][price]; ok {
												delete(b.OrderBookMAP[composedSymbol+"bids"], price)
											}
										} else {
											price, _ := strconv.ParseFloat(book[0].(string), 64)
											amount, _ := strconv.ParseFloat(book[1].(string), 64)
											b.OrderBookMAP[composedSymbol+"bids"][price] = amount
										}
									}

									var wg sync.WaitGroup

									wg.Add(1)
									go func() {
										for price, amount := range b.OrderBookMAP[composedSymbol+"bids"] {
											refLiveBook.Bids = append(refLiveBook.Bids, orderbook.Item{Price: price, Amount: amount})
										}
										sort.Sort(refLiveBook.Bids)
										wg.Done()
									}()
									wg.Add(1)
									go func() {
										for price, amount := range b.OrderBookMAP[composedSymbol+"asks"] {
											refLiveBook.Asks = append(refLiveBook.Asks, orderbook.Item{Price: price, Amount: amount})
										}
										sort.Sort(refLiveBook.Asks)
										wg.Done()
									}()

									wg.Wait()

									var totalBids, totalAsks int

									wg.Add(1)
									go func() {
										totalBids = len(refLiveBook.Bids)
										totalBids = len(refLiveBook.Bids)
										if totalBids > 20 {
											refLiveBook.Bids = refLiveBook.Bids[0:20]
											totalBids = 20
										}
										wg.Done()
									}()
									wg.Add(1)
									go func() {
										totalAsks = len(refLiveBook.Asks)
										if totalAsks > 20 {
											refLiveBook.Asks = refLiveBook.Asks[0:20]
											totalAsks = 20
										}
										wg.Done()
									}()

									wg.Wait()

									if totalBids > 0 && totalAsks > 0 {

										refMap.TotalBidLevels = totalBids
										refMap.TotalAskLevels = totalAsks
										refMap.Spread = refLiveBook.Asks[0].Price - refLiveBook.Bids[0].Price
										refMap.ProcessingTime = time.Since(start)

										refLiveBook.Stats = *refMap
										(*statsMemomory)[composedSymbol] = refMap
										(*liveBookMemomory)[composedSymbol] = refLiveBook

										serialized, err := json.Marshal((*liveBookMemomory)[composedSymbol])
										if err != nil {
											logrus.Error(b.base.Name+" ", err)
											continue
										}

										if os.Getenv("MODE") == "push" {
											if b.base.PushServer {
												if err = b.base.NanoMSG.Send(append([]byte(composedSymbol+"-"+b.base.Name+"-book"), serialized...)); err != nil {
													logrus.Error("Failed publishing: ", err.Error())
												}
											}
										} else {
											bookEncoded := &orderbook.EncodedOrderBook{}
											bookEncoded.CurrencyPair = composedSymbol
											bookEncoded.Exchange = b.base.Name
											bookEncoded.HiveTable = strings.TrimSpace(b.base.Name + "_orderbook_" + composedSymbol)
											bookEncoded.Data, err = compress.Flate(serialized, 3)
											if err != nil {
												logrus.Println(b.base.Name, " ", composedSymbol, "Problem to compress the data to send to kafka")
												continue
											}
											bookEncoded.LastUpdate = b.base.MakeTimestamp()
											serialized, err = json.Marshal(bookEncoded)
											if err != nil {
												logrus.Error(b.base.Name+" ", err)
												continue
											}

											if b.base.KafkaStreaming {
												b.base.KafkaProducer.StreamingMessage(b.base.Name+"_orderbook_"+composedSymbol, string(serialized), b.base.Verbose)
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}()
}
