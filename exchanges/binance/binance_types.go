package binance

import (
	"errors"
	"time"

	"github.com/maurodelazeri/westeros/currency/pair"
	"github.com/maurodelazeri/westeros/exchanges/ticker"
)

// ErrNotConnected is returned when the application read/writes
// a message and the connection is closed
var ErrNotConnected = errors.New("websocket: not connected")

// Responses interface to get the socket data
type Responses map[string]interface{}

// RealTimeQuote stores the socket quote
type RealTimeQuote struct {
	Pair       pair.CurrencyPair
	Tick       ticker.Price
	TickerType string
	LastUpdate time.Time
}

// RealTimeBook stores the book quotes
type RealTimeBook struct {
	Book       BinanceBook
	LastUpdate time.Time
}

// ExchangeInfo holds the full exchange information type
type ExchangeInfo struct {
	Code       int    `json:"code"`
	Msg        string `json:"msg"`
	Timezone   string `json:"timezone"`
	Servertime int64  `json:"serverTime"`
	RateLimits []struct {
		RateLimitType string `json:"rateLimitType"`
		Interval      string `json:"interval"`
		Limit         int    `json:"limit"`
	} `json:"rateLimits"`
	ExchangeFilters interface{} `json:"exchangeFilters"`
	Symbols         []struct {
		Symbol             string   `json:"symbol"`
		Status             string   `json:"status"`
		BaseAsset          string   `json:"baseAsset"`
		BaseAssetPrecision int      `json:"baseAssetPrecision"`
		QuoteAsset         string   `json:"quoteAsset"`
		QuotePrecision     int      `json:"quotePrecision"`
		OrderTypes         []string `json:"orderTypes"`
		IcebergAllowed     bool     `json:"icebergAllowed"`
		Filters            []struct {
			FilterType  string  `json:"filterType"`
			MinPrice    float64 `json:"minPrice,string"`
			MaxPrice    float64 `json:"maxPrice,string"`
			TickSize    float64 `json:"tickSize,string"`
			MinQty      float64 `json:"minQty,string"`
			MaxQty      float64 `json:"maxQty,string"`
			StepSize    float64 `json:"stepSize,string"`
			MinNotional float64 `json:"minNotional,string"`
		} `json:"filters"`
	} `json:"symbols"`
}

// OrderBookData is resp data from orderbook endpoint
type OrderBookData struct {
	Code         int           `json:"code"`
	Msg          string        `json:"msg"`
	LastUpdateID int64         `json:"lastUpdateId"`
	Bids         []interface{} `json:"bids"`
	Asks         []interface{} `json:"asks"`
}

// OrderBook actual structured data that can be used for orderbook
type OrderBook struct {
	Code int
	Msg  string
	Bids []struct {
		Price    float64
		Quantity float64
	}
	Asks []struct {
		Price    float64
		Quantity float64
	}
}

// RecentTrade holds recent trade data
type RecentTrade struct {
	Code         int     `json:"code"`
	Msg          string  `json:"msg"`
	ID           int64   `json:"id"`
	Price        float64 `json:"price,string"`
	Quantity     float64 `json:"qty,string"`
	Time         int64   `json:"time"`
	IsBuyerMaker bool    `json:"isBuyerMaker"`
	IsBestMatch  bool    `json:"isBestMatch"`
}

// HistoricalTrade holds recent trade data
type HistoricalTrade struct {
	Code         int     `json:"code"`
	Msg          string  `json:"msg"`
	ID           int64   `json:"id"`
	Price        float64 `json:"price,string"`
	Quantity     float64 `json:"qty,string"`
	Time         int64   `json:"time"`
	IsBuyerMaker bool    `json:"isBuyerMaker"`
	IsBestMatch  bool    `json:"isBestMatch"`
}

// AggregatedTrade holds aggregated trade information
type AggregatedTrade struct {
	ATradeID       int64   `json:"a"`
	Price          float64 `json:"p,string"`
	Quantity       float64 `json:"q,string"`
	FirstTradeID   int64   `json:"f"`
	LastTradeID    int64   `json:"l"`
	TimeStamp      int64   `json:"T"`
	Maker          bool    `json:"m"`
	BestMatchPrice bool    `json:"M"`
}

// CandleStick holds kline data
type CandleStick struct {
	OpenTime                 float64
	Open                     float64
	High                     float64
	Low                      float64
	Close                    float64
	Volume                   float64
	CloseTime                float64
	QuoteAssetVolume         float64
	TradeCount               float64
	TakerBuyAssetVolume      float64
	TakerBuyQuoteAssetVolume float64
}

// PriceChangeStats contains statistics for the last 24 hours trade
type PriceChangeStats struct {
	Symbol             string  `json:"symbol"`
	PriceChange        float64 `json:"priceChange,string"`
	PriceChangePercent float64 `json:"priceChangePercent,string"`
	WeightedAvgPrice   float64 `json:"weightedAvgPrice,string"`
	PrevClosePrice     float64 `json:"prevClosePrice,string"`
	LastPrice          float64 `json:"lastPrice,string"`
	LastQty            float64 `json:"lastQty,string"`
	BidPrice           float64 `json:"bidPrice,string"`
	AskPrice           float64 `json:"askPrice,string"`
	OpenPrice          float64 `json:"openPrice,string"`
	HighPrice          float64 `json:"highPrice,string"`
	LowPrice           float64 `json:"lowPrice,string"`
	Volume             float64 `json:"volume,string"`
	QuoteVolume        float64 `json:"quoteVolume,string"`
	OpenTime           int64   `json:"openTime"`
	CloseTime          int64   `json:"closeTime"`
	FirstID            int64   `json:"fristId"`
	LastID             int64   `json:"lastId"`
	Count              int64   `json:"count"`
}

// SymbolPrice holds basic symbol price
type SymbolPrice struct {
	Symbol string  `json:"symbol"`
	Price  float64 `json:"price,string"`
}

// BestPrice holds best price data
type BestPrice struct {
	Symbol   string  `json:"symbol"`
	BidPrice float64 `json:"bidPrice,string"`
	BidQty   float64 `json:"bidQty,string"`
	AskPrice float64 `json:"askPrice,string"`
	AskQty   float64 `json:"askQty,string"`
}

// NewOrderRequest request type
type NewOrderRequest struct {
	Symbol           string
	Side             string
	TradeType        string
	TimeInForce      string
	Quantity         float64
	Price            float64
	NewClientOrderID string
	StopPrice        float64
	IcebergQty       float64
	NewOrderRespType string
}

// NewOrderResponse is the return structured response from the exchange
type NewOrderResponse struct {
	Code            int     `json:"code"`
	Msg             string  `json:"msg"`
	Symbol          string  `json:"symbol"`
	OrderID         int64   `json:"orderId"`
	ClientOrderID   string  `json:"clientOrderId"`
	TransactionTime int64   `json:"transactTime"`
	Price           float64 `json:"price,string"`
	OrigQty         float64 `json:"origQty,string"`
	ExecutedQty     float64 `json:"executedQty,string"`
	Status          string  `json:"status"`
	TimeInForce     string  `json:"timeInForce"`
	Type            string  `json:"type"`
	Side            string  `json:"side"`
	Fills           []struct {
		Price           float64 `json:"price,string"`
		Qty             float64 `json:"qty,string"`
		Commission      float64 `json:"commission,string"`
		CommissionAsset float64 `json:"commissionAsset,string"`
	} `json:"fills"`
}

// QueryOrderData holds query order data
type QueryOrderData struct {
	Code          int     `json:"code"`
	Msg           string  `json:"msg"`
	Symbol        string  `json:"symbol"`
	OrderID       int64   `json:"orderId"`
	ClientOrderID string  `json:"clientOrderId"`
	Price         float64 `json:"price,string"`
	OrigQty       float64 `json:"origQty,string"`
	ExecutedQty   float64 `json:"executedQty,string"`
	Status        string  `json:"status"`
	TimeInForce   string  `json:"timeInForce"`
	Type          string  `json:"type"`
	Side          string  `json:"side"`
	StopPrice     float64 `json:"stopPrice,string"`
	IcebergQty    float64 `json:"icebergQty,string"`
	Time          int64   `json:"time"`
	IsWorking     bool    `json:"isWorking"`
}

type BinanceType struct {
	Stream string `json:"stream"`
	Data   struct {
		S string `json:"s"`
	}
}

// TickerB stores eatch ticker
type TickerB struct {
	Stream string `json:"stream"`
	Data   struct {
		E  string `json:"e"`
		EE int64  `json:"E"`
		S  string `json:"s"`
		P  string `json:"p"`
		PP string `json:"P"`
		W  string `json:"w"`
		X  string `json:"x"`
		C  string `json:"c"`
		Q  string `json:"Q"`
		B  string `json:"b"`
		BB string `json:"B"`
		A  string `json:"a"`
		AA string `json:"A"`
		O  string `json:"o"`
		H  string `json:"h"`
		L  string `json:"l"`
		V  string `json:"v"`
		QQ string `json:"q"`
		OO int64  `json:"O"`
		CC int64  `json:"C"`
		F  int    `json:"F"`
		LL int    `json:"L"`
		N  int    `json:"n"`
	} `json:"data"`
}

// Trade stores eatch trade
type Trade struct {
	HiveTable string `json:"hivetable,omitempty"`
	Stream    string `json:"stream,omitempty"`
	Data      struct {
		E  string `json:"e,omitempty"`
		EE int64  `json:"E,omitempty"`
		S  string `json:"s,omitempty"`
		T  int    `json:"t,omitempty"`
		P  string `json:"p,omitempty"`
		Q  string `json:"q,omitempty"`
		B  int    `json:"b,omitempty"`
		A  int    `json:"a,omitempty"`
		TT int64  `json:"T,omitempty"`
		M  bool   `json:"m,omitempty"`
		MM bool   `json:"M,omitempty"`
	} `json:"data,omitempty"`
}

// type TickerB struct {
// 	Stream string `json:"stream"`
// 	Data   struct {
// 		S  string      `json:"s"`
// 		C  json.Number `json:"c"`
// 		CC json.Number `json:"C"`
// 		B  json.Number `json:"b"`
// 		BB string      `json:"B"`
// 		A  string      `json:"a"`
// 		AA string      `json:"A"`
// 		H  string      `json:"h"`
// 		HH string      `json:"H"`
// 		L  json.Number `json:"l"`
// 		LL json.Number `json:"L"`
// 		//L float64 `json:"l,string"`
// 		V string `json:"v"`
// 		F int    `json:"F"`
// 	} `json:"data"`
// }

type PacketDepthUpdate struct {
	Bids []interface{} `json:"b"` // [ "price", "quantity", []]
	Asks []interface{} `json:"a"` // [ "price", "quantity", []]
}

// ExOrderbook stores the orderbook
type ExOrderbook struct {
	Stream string `json:"stream"`
	Data   struct {
		UU   int64           `json:"U"` // First update ID in event
		U    int64           `json:"u"` // Final update ID in event
		S    string          `json:"s"`
		Bids [][]interface{} `json:"b"`
		Asks [][]interface{} `json:"a"`
	} `json:"data"`
}

type BinanceBook struct {
	BookBids []BinanceBookBids `json:"bid"`
	BookAsks []BinanceBookAsks `json:"ask"`
}

type BinanceBookBids struct {
	Price float64 `json:"price,string"`
	Size  float64 `json:"size,string"`
}

type BinanceBookAsks struct {
	Price float64 `json:"price,string"`
	Size  float64 `json:"size,string"`
}

// ExchangePairConfig stores a pair config
type ExchangePairConfig []struct {
	ID                      string      `json:"id"`
	AssetCode               string      `json:"assetCode"`
	AssetName               string      `json:"assetName"`
	Unit                    string      `json:"unit"`
	TransactionFee          float64     `json:"transactionFee"`
	CommissionRate          float64     `json:"commissionRate"`
	FreeAuditWithdrawAmt    float64     `json:"freeAuditWithdrawAmt"`
	FreeUserChargeAmount    float64     `json:"freeUserChargeAmount"`
	MinProductWithdraw      string      `json:"minProductWithdraw"`
	WithdrawIntegerMultiple string      `json:"withdrawIntegerMultiple"`
	ConfirmTimes            int         `json:"confirmTimes,string"`
	CreateTime              interface{} `json:"createTime"`
	Test                    int         `json:"test"`
	URL                     string      `json:"url"`
	AddressURL              string      `json:"addressUrl"`
	BlockURL                string      `json:"blockUrl"`
	EnableCharge            bool        `json:"enableCharge"`
	EnableWithdraw          bool        `json:"enableWithdraw"`
	RegEx                   string      `json:"regEx"`
	RegExTag                string      `json:"regExTag"`
	Gas                     float64     `json:"gas"`
	ParentCode              string      `json:"parentCode"`
	IsLegalMoney            bool        `json:"isLegalMoney"`
	ReconciliationAmount    float64     `json:"reconciliationAmount"`
	SeqNum                  string      `json:"seqNum"`
	ChineseName             string      `json:"chineseName"`
	CnLink                  string      `json:"cnLink"`
	EnLink                  string      `json:"enLink"`
	LogoURL                 string      `json:"logoUrl"`
	FullLogoURL             string      `json:"fullLogoUrl"`
	ForceStatus             bool        `json:"forceStatus"`
	ResetAddressStatus      bool        `json:"resetAddressStatus"`
	ChargeDescCn            interface{} `json:"chargeDescCn"`
	ChargeDescEn            interface{} `json:"chargeDescEn"`
	AssetLabel              interface{} `json:"assetLabel"`
	SameAddress             bool        `json:"sameAddress"`
	DepositTipStatus        bool        `json:"depositTipStatus"`
	DynamicFeeStatus        bool        `json:"dynamicFeeStatus"`
	DepositTipEn            interface{} `json:"depositTipEn"`
	DepositTipCn            interface{} `json:"depositTipCn"`
	AssetLabelEn            interface{} `json:"assetLabelEn"`
	SupportMarket           interface{} `json:"supportMarket"`
	FeeReferenceAsset       string      `json:"feeReferenceAsset"`
	FeeRate                 float64     `json:"feeRate"`
	FeeDigit                int         `json:"feeDigit"`
	LegalMoney              bool        `json:"legalMoney"`
}
