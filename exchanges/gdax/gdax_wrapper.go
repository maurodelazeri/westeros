package gdax

import (
	"errors"
	"log"
	"os"

	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/currency/pair"
	exchange "github.com/maurodelazeri/westeros/exchanges"
	"github.com/maurodelazeri/westeros/exchanges/orderbook"
	"github.com/sirupsen/logrus"
)

// GetExchangePullingType return the type of pulling data the exchange is using
func (g *GDAX) GetExchangePullingType() string {
	if g.Websocket {
		return "socket"
	}
	return "rest"
}

// Start starts the GDAX go routine
func (g *GDAX) Start() {
	go g.Run()
}

// Run implements the GDAX wrapper
func (g *GDAX) Run() {
	if g.Verbose {
		log.Printf("%s Websocket: %s. (url: %s).\n", g.GetName(), common.IsEnabled(g.Websocket), gdaxWebsocketURL)
		log.Printf("%s polling delay: %ds.\n", g.GetName(), g.RESTPollingDelay)
		log.Printf("%s %d currencies enabled: %s.\n", g.GetName(), len(g.APIEnabledPairs), g.APIEnabledPairs)
	}

	mode := os.Getenv("MODE")
	if mode == "streaming" || mode == "push" {
		if g.Websocket {
			var dedicatedSocket, sharedSocket []string
			for _, pair := range g.ExchangeEnabledPairs {
				_, exist := g.StringInSlice(pair, g.WebsocketDedicated)
				if exist {
					dedicatedSocket = append(dedicatedSocket, pair)
				} else {
					sharedSocket = append(sharedSocket, pair)
				}
			}

			if len(dedicatedSocket) > 0 {
				for _, pair := range dedicatedSocket {
					socket := new(WebsocketGDAX)
					socket.base = g
					socket.subscribedPairs = append(socket.subscribedPairs, pair)
					go socket.WebsocketClient()
				}
			}

			if len(sharedSocket) > 0 {
				socket := new(WebsocketGDAX)
				socket.base = g
				socket.subscribedPairs = sharedSocket
				go socket.WebsocketClient()
			}
		}
	} else {
		logrus.Warn("Please specify a mode to start an exchange.")
		os.Exit(1)
	}

}

// GetExchangeAccountInfo retrieves balances for all enabled currencies for the
// GDAX exchange
func (g *GDAX) GetExchangeAccountInfo() (exchange.AccountInfo, error) {
	var response exchange.AccountInfo
	response.ExchangeName = g.GetName()
	accountBalance, err := g.GetAccounts()
	if err != nil {
		return response, err
	}
	for i := 0; i < len(accountBalance); i++ {
		var exchangeCurrency exchange.AccountCurrencyInfo
		exchangeCurrency.CurrencyName = accountBalance[i].Currency
		exchangeCurrency.TotalValue = accountBalance[i].Available
		exchangeCurrency.Hold = accountBalance[i].Hold

		response.Currencies = append(response.Currencies, exchangeCurrency)
	}
	return response, nil
}

// UpdateTicker updates and returns the ticker for a currency pair
func (g *GDAX) UpdateTicker() {

}

// UpdateOrderbook updates and returns the orderbook for a currency pair
func (g *GDAX) UpdateOrderbook(normalized, currency string) (orderbook.Base, error) {
	var orderBook orderbook.Base
	return orderBook, nil
}

// GetExchangeHistory returns historic trade data since exchange opening.
func (g *GDAX) GetExchangeHistory(p pair.CurrencyPair, assetType string) ([]exchange.TradeHistory, error) {
	var resp []exchange.TradeHistory

	return resp, errors.New("trade history not yet implemented")
}
