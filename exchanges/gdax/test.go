package gdax

import (
	"fmt"
	"os"

	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/maurodelazeri/westeros/common"

	"github.com/sirupsen/logrus"
)

// testing

// KafkaConsumer listem to topics on kafka
func (g *GDAX) KafkaConsumer() {
	go func() {

		broker := os.Getenv("BROKERS")
		group := os.Getenv("KAFKAGROUP")
		topics := os.Getenv("KAFKATOPICS")

		c, err := kafka.NewConsumer(&kafka.ConfigMap{
			"bootstrap.servers":               broker,
			"group.id":                        group,
			"session.timeout.ms":              6000,
			"go.events.channel.enable":        true,
			"go.application.rebalance.enable": true,
			"default.topic.config":            kafka.ConfigMap{"auto.offset.reset": "earliest"}})

		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to create consumer: %s\n", err)
			os.Exit(1)
		}

		fmt.Printf("Created Consumer %v\n", c)

		err = c.SubscribeTopics(common.SplitStrings(topics, ","), nil)

		for {
			select {
			case ev := <-c.Events():
				switch e := ev.(type) {
				case kafka.AssignedPartitions:
					fmt.Fprintf(os.Stderr, "%% %v\n", e)
					c.Assign(e.Partitions)
				case kafka.RevokedPartitions:
					fmt.Fprintf(os.Stderr, "%% %v\n", e)
					c.Unassign()
				case *kafka.Message:
					//logrus.Info(*e.TopicPartition.Topic)
					logrus.Info(e.Value)
				case kafka.PartitionEOF:
					fmt.Printf("%% Reached %v\n", e)
				case kafka.Error:
					fmt.Fprintf(os.Stderr, "%% Error: %v\n", e)
				}
			}
		}
	}()
}
