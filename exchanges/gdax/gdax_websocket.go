package gdax

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"os"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/jpillora/backoff"
	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/compress"
	"github.com/maurodelazeri/westeros/exchanges/orderbook"

	"github.com/sirupsen/logrus"
)

const (
	//gdaxWebsocketURL = "wss://ws-feed.gdax.com"
	gdaxWebsocketURL = "wss://ws-feed.pro.coinbase.com"
	//gdaxWebsocketURL = "wss://ws-feed.prime.coinbase.com"
)

// Subscribe subsribe public and private endpoints
func (g *WebsocketGDAX) Subscribe(products []string) error {
	subscribe := Message{
		Type: "subscribe",
		Channels: []MessageChannel{
			MessageChannel{
				Name:       "full",
				ProductIDs: products,
			},
			MessageChannel{
				Name:       "level2",
				ProductIDs: products,
			},
		},
	}

	json, err := common.JSONEncode(subscribe)
	if err != nil {
		return err
	}
	err = g.Conn.WriteMessage(websocket.TextMessage, json)
	if err != nil {
		return err
	}
	return nil
}

// SynnOrderBookLevel3 take a full snapshot of the orderbook to start later on be managed by the socket
func (g *WebsocketGDAX) SynnOrderBookLevel3() {

	for _, sym := range g.subscribedPairs {
		start := time.Now()

		book, err := g.base.GetOrderbookL3(sym)
		if err != nil {
			logrus.Error(g.base.Name, " Cant get the full order book level3 for: ", sym)
			continue
		}
		if len(book.Asks) == 0 || len(book.Bids) == 0 {
			logrus.Error(g.base.Name, " Cant get the full order book level3 one of the levels has 0 lenght: ", sym)
			continue
		}

		position, exist := g.base.StringInSlice(sym, g.base.ExchangeEnabledPairs)
		if exist {
			symbol := strings.Split(g.base.APIEnabledPairs[position], "/")
			composedSymbol := symbol[0] + symbol[1]
			var wg sync.WaitGroup
			wg.Add(1)
			go func(bids orderbook.BookLevelListBids) {
				for _, line := range bids {
					fields := make(map[string]interface{})
					fields["price"] = line.Price
					fields["size"] = line.Amount
					fields["side"] = "buy"
					fields["product_id"] = composedSymbol
					fields["time"] = 0
					fields["snapshot"] = 1
					fields["received"] = 0
					fields["open"] = 0
					fields["done"] = 0
					fields["match"] = 0
					fields["change"] = 0
					err := g.base.RedisServer.RedisClient.HMSet(line.ID, fields).Err()
					if err != nil {
						logrus.Error(err)
					}
				}
				wg.Done()
			}(book.Bids)

			wg.Add(1)
			go func(asks orderbook.BookLevelListAsks) {
				for _, line := range asks {
					fields := make(map[string]interface{})
					fields["price"] = line.Price
					fields["amount"] = line.Amount
					fields["side"] = "sell"
					fields["product_id"] = composedSymbol
					fields["time"] = 0
					fields["snapshot"] = 1
					fields["received"] = 0
					fields["open"] = 0
					fields["done"] = 0
					fields["match"] = 0
					fields["change"] = 0
					err := g.base.RedisServer.RedisClient.HMSet(line.ID, fields).Err()
					if err != nil {
						logrus.Error(err)
					}
				}
				wg.Done()
			}(book.Asks)

			wg.Wait()

			elapsed := time.Since(start)
			log.Printf("Order book L3 %s took %s", composedSymbol, elapsed)
		}
	}
}

// Close closes the underlying network connection without
// sending or waiting for a close frame.
func (g *WebsocketGDAX) Close() {
	g.mu.Lock()
	if g.Conn != nil {
		g.Conn.Close()
	}
	g.isConnected = false
	g.mu.Unlock()
}

func (g *WebsocketGDAX) connect() {

	bb := &backoff.Backoff{
		Min:    g.RecIntvlMin,
		Max:    g.RecIntvlMax,
		Factor: g.RecIntvlFactor,
		Jitter: true,
	}

	rand.Seed(time.Now().UTC().UnixNano())

	g.OrderBookMAP = make(map[string]map[float64]float64)
	g.OrderBookBasicStats = make(map[string]*orderbook.BookStats)
	g.LiveOrderBook = make(map[string]*orderbook.Base)
	// g.OrderBookChangeLastPosition = make(map[string]float64)

	for _, sym := range g.subscribedPairs {

		position, exist := g.base.StringInSlice(sym, g.base.ExchangeEnabledPairs)
		symbol := strings.Split(g.base.APIEnabledPairs[position], "/")
		composedSymbol := symbol[0] + symbol[1]
		if exist {

			g.LiveOrderBook[composedSymbol] = &orderbook.Base{}
			g.OrderBookBasicStats[composedSymbol] = &orderbook.BookStats{}

			g.OrderBookMAP[composedSymbol+"bids"] = make(map[float64]float64)
			g.OrderBookMAP[composedSymbol+"asks"] = make(map[float64]float64)

			// validatiom to only send to kafka/push if the change was made on levels we are interested
			// g.OrderBookChangeLastPosition[composedSymbol+"bids"] = 0
			// g.OrderBookChangeLastPosition[composedSymbol+"asks"] = 0

		}
	}

	//logrus.Info(g.base.Name, " Loading the full L3 order book ", g.subscribedPairs)
	//g.SynnOrderBookLevel3()

	for {
		nextItvl := bb.Duration()

		wsConn, httpResp, err := g.dialer.Dial(gdaxWebsocketURL, g.reqHeader)

		g.mu.Lock()
		g.Conn = wsConn
		g.dialErr = err
		g.isConnected = err == nil
		g.httpResp = httpResp
		g.mu.Unlock()

		if err == nil {
			if g.base.Verbose {
				logrus.Printf("Dial: connection was successfully established with %s\n", gdaxWebsocketURL)
			}

			err = g.Subscribe(g.subscribedPairs)
			if err != nil {
				logrus.Printf("Websocket subscription error: %s\n", err)
			}
			break
		} else {
			if g.base.Verbose {
				logrus.Println(err)
				logrus.Println("Dial: will try again in", nextItvl, "seconds.")
			}
		}

		time.Sleep(nextItvl)
	}
}

// WebsocketClient ...
func (g *WebsocketGDAX) WebsocketClient() {

	if g.RecIntvlMin == 0 {
		g.RecIntvlMin = 2 * time.Second
	}

	if g.RecIntvlMax == 0 {
		g.RecIntvlMax = 30 * time.Second
	}

	if g.RecIntvlFactor == 0 {
		g.RecIntvlFactor = 1.5
	}

	if g.HandshakeTimeout == 0 {
		g.HandshakeTimeout = 2 * time.Second
	}

	g.dialer = websocket.DefaultDialer
	g.dialer.HandshakeTimeout = g.HandshakeTimeout

	// Start reading from the socket
	g.startReading()

	go func() {
		g.connect()
	}()

	// wait on first attempt
	time.Sleep(g.HandshakeTimeout)

}

// IsConnected returns the WebSocket connection state
func (g *WebsocketGDAX) IsConnected() bool {
	g.mu.Lock()
	defer g.mu.Unlock()

	return g.isConnected
}

// CloseAndRecconect will try to reconnect.
func (g *WebsocketGDAX) closeAndRecconect() {
	g.Close()
	go func() {
		g.connect()
	}()
}

// GetHTTPResponse returns the http response from the handshake.
// Useful when WebSocket handshake fails,
// so that callers can handle redirects, authentication, etc.
func (g *WebsocketGDAX) GetHTTPResponse() *http.Response {
	g.mu.Lock()
	defer g.mu.Unlock()

	return g.httpResp
}

// GetDialError returns the last dialer error.
// nil on successful connection.
func (g *WebsocketGDAX) GetDialError() error {
	g.mu.Lock()
	defer g.mu.Unlock()

	return g.dialErr
}

// ReadMessage is a helper method for getting a reader
// using NextReader and reading from that reader to a buffer.
//
// If the connection is closed ErrNotConnected is returned
// ReadMessage initiates a websocket client
func (g *WebsocketGDAX) startReading() {
	go func() {
		for {
			select {
			default:
				if g.IsConnected() {
					err := ErrNotConnected
					msgType, resp, err := g.Conn.ReadMessage()
					if err != nil {
						logrus.Error(g.base.Name, " problem to read: ", err)
						g.closeAndRecconect()
						continue
					}

					switch msgType {
					case websocket.TextMessage:
						data := Message{}
						err := json.Unmarshal(resp, &data)
						if err != nil {
							logrus.Println(err)
							continue
						}

						position, exist := g.base.StringInSlice(data.ProductID, g.base.ExchangeEnabledPairs)
						if exist {
							symbolChannel := strings.Split(g.base.APIEnabledPairs[position], "/")
							composedSymbol := symbolChannel[0] + symbolChannel[1]

							// Sending a full copy of the streaming to kafka to be later stored on hadoop cluster
							var fullDataWait sync.WaitGroup
							if (data.Type == "snapshot") || (data.Type == "l2update") {
								// do not stream snapshot and l2update cause we already are streaming the book
							} else {
								fullDataWait.Add(1)
								go func() {
									data.HiveTable = g.base.Name + "_full_" + composedSymbol
									serialized, err := json.Marshal(data)
									if err != nil {
										logrus.Error(g.base.Name+" ", err)
									} else {
										if os.Getenv("MODE") == "streaming" {
											if g.base.KafkaStreaming {
												g.base.KafkaProducer.StreamingMessage(g.base.Name+"_full_"+composedSymbol, string(serialized), g.base.Verbose)
											}
										}
									}
									fullDataWait.Done()
								}()
							}

							statsMemomory := &g.OrderBookBasicStats
							refMap := (*statsMemomory)[composedSymbol]

							liveBookMemomory := &g.LiveOrderBook
							refLiveBook := (*liveBookMemomory)[composedSymbol]
							refLiveBook.LastUpdate = g.base.MakeTimestamp()
							refLiveBook.WeekDay = int(time.Now().Weekday())
							refLiveBook.CurrencyPair = composedSymbol
							refLiveBook.Exchange = g.base.Name

							switch data.Type {
							case "snapshot":
								var wg sync.WaitGroup
								wg.Add(1)

								go func(arr [][]string) {
									total := 0
									for _, line := range arr {
										price := g.base.Strfloat(line[0])
										amount := g.base.Strfloat(line[1])
										if total > 20 {
											continue
										}
										refLiveBook.Bids = append(refLiveBook.Bids, orderbook.Item{Price: price, Amount: amount})
										g.OrderBookMAP[composedSymbol+"bids"][price] = amount
										total++
									}
									wg.Done()
								}(data.Bids)

								wg.Add(1)
								go func(arr [][]string) {
									total := 0
									for _, line := range arr {
										price := g.base.Strfloat(line[0])
										amount := g.base.Strfloat(line[1])

										if total > 20 {
											continue
										}
										refLiveBook.Asks = append(refLiveBook.Asks, orderbook.Item{Price: price, Amount: amount})
										g.OrderBookMAP[composedSymbol+"asks"][price] = amount
										total++
									}
									wg.Done()
								}(data.Asks)

								wg.Wait()

								var totalBids, totalAsks int

								wg.Add(1)
								go func() {
									sort.Sort(refLiveBook.Bids)
									totalBids = len(refLiveBook.Bids)
									wg.Done()
								}()
								wg.Add(1)
								go func() {
									sort.Sort(refLiveBook.Asks)
									totalAsks = len(refLiveBook.Asks)
									wg.Done()
								}()

								wg.Wait()

								refMap.TotalBidLevels = totalBids
								refMap.TotalAskLevels = totalAsks

								// g.OrderBookChangeLastPosition[composedSymbol+"bids"] = refLiveBook.Bids[totalBids-1].Price
								// g.OrderBookChangeLastPosition[composedSymbol+"asks"] = refLiveBook.Asks[totalAsks-1].Price

								refLiveBook.Stats = *refMap
								(*statsMemomory)[composedSymbol] = refMap
								(*liveBookMemomory)[composedSymbol] = refLiveBook

							case "l2update":
								start := time.Now()

								for _, data := range data.Changes {
									switch data[0] {
									case "buy":
										if data[2] == "0" {
											price := g.base.Strfloat(data[1])
											if _, ok := g.OrderBookMAP[composedSymbol+"bids"][price]; ok {
												delete(g.OrderBookMAP[composedSymbol+"bids"], price)
											}
										} else {
											price := g.base.Strfloat(data[1])
											amount := g.base.Strfloat(data[2])
											g.OrderBookMAP[composedSymbol+"bids"][price] = amount
										}
									case "sell":
										if data[2] == "0" {
											price := g.base.Strfloat(data[1])
											if _, ok := g.OrderBookMAP[composedSymbol+"asks"][price]; ok {
												delete(g.OrderBookMAP[composedSymbol+"asks"], price)
											}
										} else {
											price := g.base.Strfloat(data[1])
											amount := g.base.Strfloat(data[2])
											g.OrderBookMAP[composedSymbol+"asks"][price] = amount
										}
									default:
										continue
									}
								}

								var wg sync.WaitGroup

								wg.Add(1)
								go func() {
									refLiveBook.Bids = orderbook.BookLevelListBids{}
									for price, amount := range g.OrderBookMAP[composedSymbol+"bids"] {
										refLiveBook.Bids = append(refLiveBook.Bids, orderbook.Item{Price: price, Amount: amount})
									}
									sort.Sort(refLiveBook.Bids)
									wg.Done()
								}()
								wg.Add(1)
								go func() {
									refLiveBook.Asks = orderbook.BookLevelListAsks{}
									for price, amount := range g.OrderBookMAP[composedSymbol+"asks"] {
										refLiveBook.Asks = append(refLiveBook.Asks, orderbook.Item{Price: price, Amount: amount})
									}
									sort.Sort(refLiveBook.Asks)
									wg.Done()
								}()

								wg.Wait()

								var totalBids, totalAsks int

								wg.Add(1)
								go func() {
									totalBids = len(refLiveBook.Bids)
									if totalBids > 20 {
										refLiveBook.Bids = refLiveBook.Bids[0:20]
										totalBids = 20
									}
									wg.Done()
								}()
								wg.Add(1)
								go func() {
									totalAsks = len(refLiveBook.Asks)
									if totalAsks > 20 {
										refLiveBook.Asks = refLiveBook.Asks[0:20]
										totalAsks = 20
									}
									wg.Done()
								}()

								wg.Wait()

								if totalBids > 0 && totalAsks > 0 {

									refMap.TotalBidLevels = totalBids
									refMap.TotalAskLevels = totalAsks
									refMap.Spread = refLiveBook.Asks[0].Price - refLiveBook.Bids[0].Price
									refMap.ProcessingTime = time.Since(start)
									refLiveBook.Stats = *refMap
									(*statsMemomory)[composedSymbol] = refMap
									(*liveBookMemomory)[composedSymbol] = refLiveBook

									//compress the data
									serialized, err := json.Marshal(refLiveBook)
									if err != nil {
										logrus.Error(g.base.Name+" ", err)
										continue
									}

									// asksUpdate := g.OrderBookChangeLastPosition[composedSymbol+"bids"][false]
									// bidsUpdate := g.OrderBookChangeLastPosition[composedSymbol+"asks"][false]

									if os.Getenv("MODE") == "push" {
										if g.base.PushServer {
											if err = g.base.NanoMSG.Send(append([]byte(composedSymbol+"-"+g.base.Name+"-book"), serialized...)); err != nil {
												logrus.Error("Failed publishing: ", err.Error())
											}
										}
									} else {

										// Separation of store and live
										bookEncoded := &orderbook.EncodedOrderBook{}
										bookEncoded.CurrencyPair = composedSymbol
										bookEncoded.Exchange = g.base.Name
										bookEncoded.HiveTable = strings.TrimSpace(g.base.Name + "_orderbook_" + composedSymbol)
										bookEncoded.Data, err = compress.Flate(serialized, 9)

										if err != nil {
											logrus.Println(g.base.Name, " ", composedSymbol, "Problem to compress the data to send to kafka")
											continue
										}
										bookEncoded.LastUpdate = g.base.MakeTimestamp()
										serialized, err = json.Marshal(bookEncoded)
										if err != nil {
											logrus.Error(g.base.Name+" ", err)
											continue
										}

										if g.base.KafkaStreaming {
											g.base.KafkaProducer.StreamingMessage(g.base.Name+"_orderbook_"+composedSymbol, string(serialized), g.base.Verbose)
										}
									}
								}

							case "received":
								// data.HiveTable = g.base.Name + "_received_" + composedSymbol
								// serialized, _ := json.Marshal(data)
								// if g.base.KafkaStreaming {
								// 	g.base.KafkaProducer.StreamingMessage(g.base.Name+"_message_"+composedSymbol, string(serialized), g.base.Verbose)
								// }

							case "open":
								// data.HiveTable = g.base.Name + "open" + composedSymbol
								// serialized, _ := json.Marshal(data)
								// if g.base.KafkaStreaming {
								// 	g.base.KafkaProducer.StreamingMessage(g.base.Name+"_message_"+composedSymbol, string(serialized), g.base.Verbose)
								// }

							case "done":
								// data.HiveTable = g.base.Name + "done" + composedSymbol
								// serialized, _ := json.Marshal(data)
								// if g.base.KafkaStreaming {
								// 	g.base.KafkaProducer.StreamingMessage(g.base.Name+"_message_"+composedSymbol, string(serialized), g.base.Verbose)
								// }

							case "match":
								data.HiveTable = strings.TrimSpace(g.base.Name + "_trade_" + composedSymbol)
								if refMap.LastDealPrice > 0 {
									if refMap.LastDealPrice == data.Price {
										refMap.TotalTradesBeforePriceImprovement++
										refMap.TotalSizeBeforePriceImprovement = refMap.TotalSizeBeforePriceImprovement + data.Size
										refMap.Improvement = false
									} else {
										refMap.TotalSizeBeforePriceImprovement = data.Size
										refMap.TotalTradesBeforePriceImprovement = 1
										refMap.Improvement = true

										refMap.PercentagePriceImprovement = g.base.PercentChange(refMap.LastDealPrice, data.Price)

										refMap.WaitingTimeTillPriceImprovement = time.Since(time.Unix(0, refMap.LastDealTimestamp)).Nanoseconds()
									}

									refMap.LastDealPrice = data.Price
									refMap.LastDealAmount = data.Size
									refMap.LastDealSide = data.Side
									refMap.LastDealTimestamp = g.base.MakeTimestamp()

									(*statsMemomory)[composedSymbol] = refMap

									serialized, err := json.Marshal(data)
									if err != nil {
										logrus.Error(g.base.Name+" ", err)
										continue
									}

									if os.Getenv("MODE") == "push" {
										if g.base.PushServer {
											if err = g.base.NanoMSG.Send(append([]byte(composedSymbol+"-"+g.base.Name+"-trade"), serialized...)); err != nil {
												logrus.Error("Failed publishing: ", err.Error())
											}
										}
									} else {
										if g.base.KafkaStreaming {
											g.base.KafkaProducer.StreamingMessage(g.base.Name+"_trade_"+composedSymbol, string(serialized), g.base.Verbose)
										}
									}

								} else {
									refMap.LastDealPrice = data.Price
									refMap.LastDealAmount = data.Size
									refMap.LastDealSide = data.Side
									refMap.LastDealTimestamp = g.base.MakeTimestamp()

									if data.Side == "buy" {
										refMap.SideImprovment = 1
									} else {
										refMap.SideImprovment = 0
									}

									(*statsMemomory)[composedSymbol] = refMap
								}
							case "change":
								// data.HiveTable = g.base.Name + "change" + composedSymbol
								// serialized, _ := json.Marshal(data)
								// if g.base.KafkaStreaming {
								// 	g.base.KafkaProducer.StreamingMessage(g.base.Name+"_message"+composedSymbol, string(serialized), g.base.Verbose)
								// }
							}

							// Wait for the full data sent to kafka, probably
							fullDataWait.Wait()
						}

					}
				}
			}
		}
	}()
}
