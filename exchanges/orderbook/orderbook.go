package orderbook

import (
	"errors"
	"time"

	"github.com/maurodelazeri/westeros/currency/pair"
)

// Const values for orderbook package
const (
	ErrOrderbookForExchangeNotFound = "Ticker for exchange does not exist."
	ErrPrimaryCurrencyNotFound      = "Error primary currency for orderbook not found."
	ErrSecondaryCurrencyNotFound    = "Error secondary currency for orderbook not found."

	Spot = "SPOT"
)

// Vars for the orderbook package
var (
	Orderbooks []Orderbook
)

// EncodedOrderBook stores the orderbook encoded base64
type EncodedOrderBook struct {
	HiveTable    string `json:"hivetable"`
	Exchange     string `json:"Exchange"`
	CurrencyPair string `json:"CurrencyPair"`
	LastUpdate   int64  `json:"LastUpdate"`
	Data         []byte `json:"Data"`
}

// Base holds the fields for the orderbook base
type Base struct {
	Sequence     int64             `json:"sequence"`
	Exchange     string            `json:"Exchange"`
	CurrencyPair string            `json:"CurrencyPair"`
	Bids         BookLevelListBids `json:"bids"`
	Asks         BookLevelListAsks `json:"asks"`
	LastUpdate   int64             `json:"last_update"`
	WeekDay      int               `json:"weekday"` // day of the week represented by numbers
	Stats        BookStats         `json:"stats"`
}

// BookStats holds statistics from levels
type BookStats struct {
	TotalBidLevels                    int
	TotalAskLevels                    int
	LastDealPrice                     float64
	LastDealAmount                    float64
	LastDealSide                      string
	LastDealTimestamp                 int64
	Improvement                       bool
	WaitingTimeTillPriceImprovement   int64   // how long took to the price move
	PercentagePriceImprovement        float64 // how much the price was improved
	SideImprovment                    int64   // 1 buy 0 sell - it keep track of consecutive improvement on the same side, basically trending
	TotalTradesBeforePriceImprovement int64   // trades total before the price improvement
	TotalSizeBeforePriceImprovement   float64 // size total traded before the price improvement
	Spread                            float64
	ProcessingTime                    time.Duration
}

// BookLevelListBids a item of the book
type BookLevelListBids []Item

// BookLevelListAsks a item of the book
type BookLevelListAsks []Item

func (a BookLevelListBids) Len() int           { return len(a) }
func (a BookLevelListBids) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a BookLevelListBids) Less(i, j int) bool { return a[i].Price > a[j].Price }

func (a BookLevelListAsks) Len() int           { return len(a) }
func (a BookLevelListAsks) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a BookLevelListAsks) Less(i, j int) bool { return a[i].Price < a[j].Price }

// Item stores the amount and price values
type Item struct {
	ID     string  `json:"id,omitempty"`
	Amount float64 `json:"amount,omitempty"`
	Price  float64 `json:"price,omitempty"`
}

// LevelIndividualOrdersOrderBook holds the individuals orders and in addition stats
type LevelIndividualOrdersOrderBook struct {
	ID     string  `json:"id,omitempty"`
	Amount float64 `json:"amount,omitempty"`
	Price  float64 `json:"price,omitempty"`
}

// LevelStatsOrderBook holds level statistics
type LevelStatsOrderBook struct {
	AverageSize           float64
	LifeCreationTimestamp int64
	LifeRemovalTimestamp  int64
	TotalIndividualOrders int64
}

// BookAgregationStats holds whole book statistics
type BookAgregationStats struct {
	ImbalanceBook            float64
	GASP                     float64
	VWAP                     float64
	Pressure                 []float64
	ImediateMarketOrderCost  []float64
	VolatilityEstimation     float64
	AcumutationDistribuition []float64
	MiddlePrice              float64
	SmartPrice               float64 // A variation on mid-price where the average of the bid and ask prices is weighted according to their inverse volume
	Spread                   float64
}

// Orderbook holds the orderbook information for a currency pair and type
type Orderbook struct {
	Orderbook    map[pair.CurrencyItem]map[pair.CurrencyItem]map[string]Base
	ExchangeName string
}

// CalculateTotalBids returns the total amount of bids and the total orderbook
// bids value
func (o *Base) CalculateTotalBids() (float64, float64) {
	amountCollated := float64(0)
	total := float64(0)
	for _, x := range o.Bids {
		amountCollated += x.Amount
		total += x.Amount * x.Price
	}
	return amountCollated, total
}

// CalculateTotalAsks returns the total amount of asks and the total orderbook
// asks value
func (o *Base) CalculateTotalAsks() (float64, float64) {
	amountCollated := float64(0)
	total := float64(0)
	for _, x := range o.Asks {
		amountCollated += x.Amount
		total += x.Amount * x.Price
	}
	return amountCollated, total
}

// GetOrderbook checks and returns the orderbook given an exchange name and
// currency pair if it exists
func GetOrderbook(exchange string, p pair.CurrencyPair, orderbookType string) (Base, error) {
	orderbook, err := GetOrderbookByExchange(exchange)
	if err != nil {
		return Base{}, err
	}

	if !FirstCurrencyExists(exchange, p.GetFirstCurrency()) {
		return Base{}, errors.New(ErrPrimaryCurrencyNotFound)
	}

	if !SecondCurrencyExists(exchange, p) {
		return Base{}, errors.New(ErrSecondaryCurrencyNotFound)
	}

	return orderbook.Orderbook[p.GetFirstCurrency()][p.GetSecondCurrency()][orderbookType], nil
}

// GetOrderbookByExchange returns an exchange orderbook
func GetOrderbookByExchange(exchange string) (*Orderbook, error) {
	for _, y := range Orderbooks {
		if y.ExchangeName == exchange {
			return &y, nil
		}
	}
	return nil, errors.New(ErrOrderbookForExchangeNotFound)
}

// FirstCurrencyExists checks to see if the first currency of the orderbook map
// exists
func FirstCurrencyExists(exchange string, currency pair.CurrencyItem) bool {
	for _, y := range Orderbooks {
		if y.ExchangeName == exchange {
			if _, ok := y.Orderbook[currency]; ok {
				return true
			}
		}
	}
	return false
}

// SecondCurrencyExists checks to see if the second currency of the orderbook
// map exists
func SecondCurrencyExists(exchange string, p pair.CurrencyPair) bool {
	for _, y := range Orderbooks {
		if y.ExchangeName == exchange {
			if _, ok := y.Orderbook[p.GetFirstCurrency()]; ok {
				if _, ok := y.Orderbook[p.GetFirstCurrency()][p.GetSecondCurrency()]; ok {
					return true
				}
			}
		}
	}
	return false
}

// CreateNewOrderbook creates a new orderbook
func CreateNewOrderbook(exchangeName string, p pair.CurrencyPair, orderbookNew Base, orderbookType string) Orderbook {
	orderbook := Orderbook{}
	orderbook.ExchangeName = exchangeName
	orderbook.Orderbook = make(map[pair.CurrencyItem]map[pair.CurrencyItem]map[string]Base)
	a := make(map[pair.CurrencyItem]map[string]Base)
	b := make(map[string]Base)
	b[orderbookType] = orderbookNew
	a[p.SecondCurrency] = b
	orderbook.Orderbook[p.FirstCurrency] = a
	Orderbooks = append(Orderbooks, orderbook)
	return orderbook
}

// ProcessOrderbook processes incoming orderbooks, creating or updating the
// Orderbook list
func ProcessOrderbook(exchangeName string, p pair.CurrencyPair, orderbookNew Base, orderbookType string) {
	orderbookNew.CurrencyPair = p.Pair().String()
	orderbookNew.LastUpdate = time.Now().UnixNano()

	if len(Orderbooks) == 0 {
		CreateNewOrderbook(exchangeName, p, orderbookNew, orderbookType)
		return
	}

	orderbook, err := GetOrderbookByExchange(exchangeName)
	if err != nil {
		CreateNewOrderbook(exchangeName, p, orderbookNew, orderbookType)
		return
	}

	if FirstCurrencyExists(exchangeName, p.GetFirstCurrency()) {
		if !SecondCurrencyExists(exchangeName, p) {
			a := orderbook.Orderbook[p.FirstCurrency]
			b := make(map[string]Base)
			b[orderbookType] = orderbookNew
			a[p.SecondCurrency] = b
			orderbook.Orderbook[p.FirstCurrency] = a
			return
		}
	}

	a := make(map[pair.CurrencyItem]map[string]Base)
	b := make(map[string]Base)
	b[orderbookType] = orderbookNew
	a[p.SecondCurrency] = b
	orderbook.Orderbook[p.FirstCurrency] = a
}
