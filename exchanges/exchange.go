package exchange

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/config"
	"github.com/maurodelazeri/westeros/currency/pair"
	"github.com/maurodelazeri/westeros/exchanges/nonce"
	"github.com/maurodelazeri/westeros/exchanges/orderbook"
	"github.com/maurodelazeri/westeros/kafka"
	"github.com/maurodelazeri/westeros/redis"
	mangos "nanomsg.org/go-mangos"
)

const (
	warningBase64DecryptSecretKeyFailed = "WARNING -- Exchange %s unable to base64 decode secret key.. Disabling Authenticated API support."

	// WarningAuthenticatedRequestWithoutCredentialsSet error message for authenticated request without credentials set
	WarningAuthenticatedRequestWithoutCredentialsSet = "WARNING -- Exchange %s authenticated HTTP request called but not supported due to unset/default API keys."
	// ErrExchangeNotFound is a constant for an error message
	ErrExchangeNotFound = "Exchange not found in dataset."
)

// AccountInfo is a Generic type to hold each exchange's holdings in
// all enabled currencies
type AccountInfo struct {
	ExchangeName string
	Currencies   []AccountCurrencyInfo
}

// AccountCurrencyInfo is a sub type to store currency name and value
type AccountCurrencyInfo struct {
	CurrencyName string
	TotalValue   float64
	Hold         float64
}

// TradeHistory holds exchange history data
type TradeHistory struct {
	Timestamp int64
	TID       int64
	Price     float64
	Amount    float64
	Exchange  string
	Type      string
}

// NewOrder stores a order request
type NewOrder struct {
	OrderID string  `json:"orderid"`
	Product string  `json:"product"`
	Size    float64 `json:"size,string"`
	Price   float64 `json:"price,string"`
	Side    string  `json:"side"`
}

// EnabledExchangeOrderbooks stores all order books
type EnabledExchangeOrderbooks struct {
	ExchangeName   string           `json:"exchangeName"`
	ExchangeValues []orderbook.Base `json:"exchangeValues"`
}

// AllEnabledExchangeOrderbooks holds the enabled exchange orderbooks
type AllEnabledExchangeOrderbooks struct {
	Data []EnabledExchangeOrderbooks `json:"data"`
}

// Base stores the individual exchange information
type Base struct {
	Name                        string
	Enabled                     bool
	Verbose                     bool
	Websocket                   bool
	WebsocketDedicated          []string
	KafkaStreaming              bool
	PushServer                  bool
	RESTPollingDelay            time.Duration
	AuthenticatedAPISupport     bool
	APISecret, APIKey, ClientID string
	Nonce                       nonce.Nonce
	TakerFee, MakerFee, Fee     float64
	BaseCurrencies              []string
	ExchangeEnabledPairs        []string
	APIEnabledPairs             []string
	AssetTypes                  []string
	Markets                     map[string][]string
	WebsocketURL                string
	APIUrl                      string
	RedisServer                 *redisserver.RedisServer
	KafkaProducer               *kafkaserver.KafkaProducer
	NanoMSG                     mangos.Socket
}

// PairExchangeInternalConfig stores the individual par config from an exchange
type PairExchangeInternalConfig struct {
	Enabled    bool
	Product    string
	MakerFee   float64
	TakerFee   float64
	Deposit    DepositConfig
	Withdrawal WithdrawalConfig
	LastUpdate time.Time
}

// DepositConfig stores deposit config
type DepositConfig struct {
	Enabled       bool
	Confirmations int
	ExchangeFee   float64
	PercentageFee bool
}

// WithdrawalConfig stores withdrawal config
type WithdrawalConfig struct {
	Enabled       bool
	ExchangeFee   float64
	NetworkFee    float64
	PercentageFee bool
}

// IWesterosExchange enforces standard functions for all exchanges supported in
// GoCryptoTrader
type IWesterosExchange interface {
	Setup(exch config.ExchangeConfig)
	Start()
	SetDefaults()
	GetName() string
	IsEnabled() bool
	SetEnabled(bool)
	UpdateTicker()
	UpdateOrderbook(normalized, currency string) (orderbook.Base, error)
	GetExchangeAccountInfo() (AccountInfo, error)
	GetAuthenticatedAPISupport() bool
	GetExchangeHistory(pair.CurrencyPair, string) ([]TradeHistory, error)
	GetExchangePullingType() string
}

// GetExchangeAssetTypes returns the asset types the exchange supports (SPOT,
// binary, futures)
func GetExchangeAssetTypes(exchName string) ([]string, error) {
	cfg := config.GetConfig()
	exch, err := cfg.GetExchangeConfig(exchName)
	if err != nil {
		return nil, err
	}

	return common.SplitStrings(exch.AssetTypes, ","), nil
}

// GetAuthenticatedAPISupport returns whether the exchange supports
// authenticated API requests
func (e *Base) GetAuthenticatedAPISupport() bool {
	return e.AuthenticatedAPISupport
}

// GetName is a method that returns the name of the exchange base
func (e *Base) GetName() string {
	return e.Name
}

// SetEnabled is a method that sets if the exchange is enabled
func (e *Base) SetEnabled(enabled bool) {
	e.Enabled = enabled
}

// IsEnabled is a method that returns if the current exchange is enabled
func (e *Base) IsEnabled() bool {
	return e.Enabled
}

// SetAPIKeys is a method that sets the current API keys for the exchange
func (e *Base) SetAPIKeys(APIKey, APISecret, ClientID string, b64Decode bool) {
	if !e.AuthenticatedAPISupport {
		return
	}

	e.APIKey = APIKey
	e.ClientID = ClientID

	if b64Decode {
		result, err := common.Base64Decode(APISecret)
		if err != nil {
			e.AuthenticatedAPISupport = false
			log.Printf(warningBase64DecryptSecretKeyFailed, e.Name)
		}
		e.APISecret = string(result)
	} else {
		e.APISecret = APISecret
	}
}

// StringInSlice returns position of string, of exist
func (e *Base) StringInSlice(a string, list []string) (int, bool) {
	for index, b := range list {
		if b == a {
			return index, true
		}
	}
	return 0, false
}

// FloatInSlice returns position of float64, of exist
func (e *Base) FloatInSlice(a float64, list []float64) (int, bool) {
	for index, b := range list {
		if b == a {
			return index, true
		}
	}
	return 0, false
}

// IntnSlice returns position of string, of exist
func (e *Base) IntnSlice(a int64, list []int64) (int, bool) {
	for index, b := range list {
		if b == a {
			return index, true
		}
	}
	return 0, false
}

// RemoveIndex removes an index from a float array
func (e *Base) RemoveIndex(s []float64, index int) []float64 {
	return append(s[:index], s[index+1:]...)
}

// FloatToString to convert a float number to a string
func (e *Base) FloatToString(number float64) string {
	return strconv.FormatFloat(number, 'f', -1, 64)
}

func (e *Base) MakeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Microsecond)
}

// PercentChange gives the difference in percentage from 2 numbers
func (e *Base) PercentChange(base float64, current float64) float64 {
	if base > 0 && current > 0 {
		absolute := base - current
		if absolute < 0 {
			absolute = absolute * -1
		}
		average := ((base + current) / 2) / 2
		result := absolute / average
		formated := fmt.Sprintf("%.4f", result)
		return e.Strfloat(formated)
	}
	return -1
}

// Let's use 20 and 30 as an example. We will need to divide the absolute difference by the average of those two numbers and express it as percentages.

// find the absolute difference between the two: |20 - 30| = |-10| = 10
// find the average of those two numbers: (20 + 30) / 2 = 50 / 2 = 25
// divide those two: 10 / 25 = 0.4
// express it as percentages: 0.4 * 100 = 40%

func (e *Base) Strfloat(i string) float64 {
	f, _ := strconv.ParseFloat(i, 64)
	return f
}
