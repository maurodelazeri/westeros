package bitfinex

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"os"
	"reflect"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/jpillora/backoff"
	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/compress"
	"github.com/maurodelazeri/westeros/exchanges/orderbook"
	"github.com/sirupsen/logrus"
)

// WebsocketSubscribe subscribes to the websocket channel
func (b *WebsocketBitfinex) WebsocketSubscribe(channel string, params map[string]string) error {
	request := make(map[string]string)
	request["event"] = "subscribe"
	request["channel"] = channel

	if len(params) > 0 {
		for k, v := range params {
			request[k] = v
		}
	}
	return b.WebsocketSend(request)
}

// WebsocketSend sends data to the websocket server
func (b *WebsocketBitfinex) WebsocketSend(data interface{}) error {
	json, err := common.JSONEncode(data)
	if err != nil {
		return err
	}

	return b.Conn.WriteMessage(websocket.TextMessage, json)
}

// WebsocketAddSubscriptionChannel adds a new subscription channel to the
// WebsocketSubdChannels map in bitfinex.go (Bitfinex struct)
func (b *WebsocketBitfinex) WebsocketAddSubscriptionChannel(chanID int, channel, pair string) {
	chanInfo := WebsocketChanInfo{Pair: pair, Channel: channel}
	b.WebsocketSubdChannels[chanID] = chanInfo
	if b.base.Verbose {
		log.Printf("Subscribed to Channel: %s Pair: %s ChannelID: %d\n", channel, pair, chanID)
	}
}

// Close closes the underlying network connection without
// sending or waiting for a close frame.
func (b *WebsocketBitfinex) Close() {
	b.mu.Lock()
	if b.Conn != nil {
		b.Conn.Close()
	}
	b.isConnected = false
	b.mu.Unlock()
}

func (b *WebsocketBitfinex) connect() {

	bb := &backoff.Backoff{
		Min:    b.RecIntvlMin,
		Max:    b.RecIntvlMax,
		Factor: b.RecIntvlFactor,
		Jitter: true,
	}

	rand.Seed(time.Now().UTC().UnixNano())

	b.WebsocketSubdChannels = make(map[int]WebsocketChanInfo)

	b.OrderBookMAP = make(map[string]map[float64]float64)
	b.OrderBookBasicStats = make(map[string]*orderbook.BookStats)
	b.LiveOrderBook = make(map[string]*orderbook.Base)

	for _, sym := range b.subscribedPairs {

		position, exist := b.base.StringInSlice(sym, b.base.ExchangeEnabledPairs)
		symbol := strings.Split(b.base.APIEnabledPairs[position], "/")
		composedSymbol := symbol[0] + symbol[1]
		if exist {

			b.OrderBookBasicStats[composedSymbol] = &orderbook.BookStats{}

			b.OrderBookMAP[composedSymbol+"bids"] = make(map[float64]float64)
			b.OrderBookMAP[composedSymbol+"asks"] = make(map[float64]float64)

			// err := b.base.RedisServer.RedisClient.Del("stats" + composedSymbol + b.base.Name).Err()
			// if err != nil {
			// 	logrus.Error(err)
			// }
		}
	}

	for {
		nextItvl := bb.Duration()

		wsConn, httpResp, err := b.dialer.Dial(b.url, b.reqHeader)

		b.mu.Lock()
		b.Conn = wsConn
		b.dialErr = err
		b.isConnected = err == nil
		b.httpResp = httpResp
		b.mu.Unlock()

		if err == nil {
			if b.base.Verbose {
				log.Printf("Dial: connection was successfully established with %s\n", b.url)
			}

			channels := []string{"book", "trades"}
			for _, x := range channels {
				for _, y := range b.subscribedPairs {
					params := make(map[string]string)
					if x == "book" {
						params["prec"] = "P0"
					}
					params["pair"] = y
					b.WebsocketSubscribe(x, params)
				}
			}

			break
		} else {
			if b.base.Verbose {
				log.Println(err)
				log.Println("Dial: will try again in", nextItvl, "seconds.")
			}
		}

		time.Sleep(nextItvl)
	}
}

func (b *WebsocketBitfinex) WebsocketClient() {

	if b.RecIntvlMin == 0 {
		b.RecIntvlMin = 2 * time.Second
	}

	if b.RecIntvlMax == 0 {
		b.RecIntvlMax = 20 * time.Second
	}

	if b.RecIntvlFactor == 0 {
		b.RecIntvlFactor = 1.5
	}

	if b.HandshakeTimeout == 0 {
		b.HandshakeTimeout = 2 * time.Second
	}

	b.dialer = websocket.DefaultDialer
	b.dialer.HandshakeTimeout = b.HandshakeTimeout

	// Start reading from the socket if connected
	b.startReading()

	go func() {
		b.connect()
	}()

	// wait on first attempt
	time.Sleep(b.HandshakeTimeout)

}

// IsConnected returns the WebSocket connection state
func (b *WebsocketBitfinex) IsConnected() bool {
	b.mu.Lock()
	defer b.mu.Unlock()

	return b.isConnected
}

// CloseAndRecconect will try to reconnect.
func (b *WebsocketBitfinex) closeAndRecconect() {
	b.Close()
	go func() {
		b.connect()
	}()
}

// GetHTTPResponse returns the http response from the handshake.
// Useful when WebSocket handshake fails,
// so that callers can handle redirects, authentication, etc.
func (b *WebsocketBitfinex) GetHTTPResponse() *http.Response {
	b.mu.Lock()
	defer b.mu.Unlock()

	return b.httpResp
}

// GetDialError returns the last dialer error.
// nil on successful connection.
func (b *WebsocketBitfinex) GetDialError() error {
	b.mu.Lock()
	defer b.mu.Unlock()

	return b.dialErr
}

// ReadMessage is a helper method for getting a reader
// using NextReader and reading from that reader to a buffer.
//
// If the connection is closed ErrNotConnected is returned
// ReadMessage initiates a websocket client
func (b *WebsocketBitfinex) startReading() {
	go func() {
		for {
			select {
			default:
				if b.IsConnected() {
					err := ErrNotConnected
					msgType, resp, err := b.Conn.ReadMessage()
					if err != nil {
						logrus.Error(b.Name, " problem to read: ", err)
						b.closeAndRecconect()
						continue
					}
					switch msgType {
					case websocket.TextMessage:

						var result interface{}
						err := common.JSONDecode(resp, &result)
						if err != nil {
							log.Println("Ops ", err)
							continue
						}
						switch reflect.TypeOf(result).String() {
						case "map[string]interface {}":
							eventData := result.(map[string]interface{})
							event := eventData["event"]

							switch event {
							case "subscribed":
								b.WebsocketAddSubscriptionChannel(int(eventData["chanId"].(float64)), eventData["channel"].(string), eventData["pair"].(string))
							case "auth":
								status := eventData["status"].(string)

								if status == "OK" {
									b.WebsocketAddSubscriptionChannel(0, "account", "N/A")
								} else if status == "fail" {
									log.Printf("%s Websocket unable to AUTH", eventData["code"].(string))
								}
							}
						case "[]interface {}":
							chanData := result.([]interface{})
							chanID := int(chanData[0].(float64))
							chanInfo, ok := b.WebsocketSubdChannels[chanID]
							if !ok {
								log.Printf("Unable to locate chanID: %d\n", chanID)
							} else {
								if len(chanData) == 2 {
									if reflect.TypeOf(chanData[1]).String() == "string" {
										if chanData[1].(string) == "hb" {
											continue
										}
									}
								}
							}
							switch chanInfo.Channel {
							case "unknown":
								logrus.Error(b.Name, " Problem subscribing channel, wrong name")
							case "book":
								position, exist := b.StringInSlice(chanInfo.Pair, b.base.ExchangeEnabledPairs)
								if exist {
									symbolChannel := strings.Split(b.base.APIEnabledPairs[position], "/")

									composedSymbol := symbolChannel[0] + symbolChannel[1]
									statsMemomory := &b.OrderBookBasicStats
									refMap := (*statsMemomory)[composedSymbol]

									liveBookMemomory := &b.LiveOrderBook
									refLiveBook := (*liveBookMemomory)[composedSymbol]
									refLiveBook = &orderbook.Base{}
									refLiveBook.LastUpdate = b.base.MakeTimestamp()
									refLiveBook.WeekDay = int(time.Now().Weekday())
									refLiveBook.CurrencyPair = composedSymbol
									refLiveBook.Exchange = b.base.Name
									start := time.Now()

									switch len(chanData) {
									case 2:
										data := chanData[1].([]interface{})
										// Snapshoot
										if len(data) > 10 {
											for _, x := range data {
												y := x.([]interface{})
												if y[2].(float64) > 0 {
													price := y[0].(float64)
													amount := y[2].(float64)
													b.OrderBookMAP[composedSymbol+"bids"][price] = amount
												} else {
													price := y[0].(float64)
													amount := y[2].(float64) * -1
													b.OrderBookMAP[composedSymbol+"asks"][price] = amount
												}
											}
										} else {
											// book updates
											y := data
											if y[2].(float64) > 0 {
												if y[1].(float64) == 0 {
													if _, ok := b.OrderBookMAP[composedSymbol+"bids"][y[0].(float64)]; ok {
														delete(b.OrderBookMAP[composedSymbol+"bids"], y[0].(float64))
													}
												} else {
													b.OrderBookMAP[composedSymbol+"bids"][y[0].(float64)] = y[2].(float64)
												}
											} else {
												if y[1].(float64) == 0 {
													if _, ok := b.OrderBookMAP[composedSymbol+"asks"][y[0].(float64)]; ok {
														delete(b.OrderBookMAP[composedSymbol+"asks"], y[0].(float64))
													}
												} else {
													b.OrderBookMAP[composedSymbol+"asks"][y[0].(float64)] = y[2].(float64) * -1
												}
											}

										}
									}

									var wg sync.WaitGroup

									wg.Add(1)
									go func() {
										for price, amount := range b.OrderBookMAP[composedSymbol+"bids"] {
											refLiveBook.Bids = append(refLiveBook.Bids, orderbook.Item{Price: price, Amount: amount})
										}
										sort.Sort(refLiveBook.Bids)
										wg.Done()
									}()
									wg.Add(1)
									go func() {
										for price, amount := range b.OrderBookMAP[composedSymbol+"asks"] {
											refLiveBook.Asks = append(refLiveBook.Asks, orderbook.Item{Price: price, Amount: amount})
										}
										sort.Sort(refLiveBook.Asks)
										wg.Done()
									}()

									wg.Wait()

									var totalBids, totalAsks int

									wg.Add(1)
									go func() {
										totalBids = len(refLiveBook.Bids)
										if totalBids > 20 {
											refLiveBook.Bids = refLiveBook.Bids[0:20]
											totalBids = 20
										}
										wg.Done()
									}()
									wg.Add(1)
									go func() {
										totalAsks = len(refLiveBook.Asks)
										if totalAsks > 20 {
											refLiveBook.Asks = refLiveBook.Asks[0:20]
											totalAsks = 20
										}
										wg.Done()
									}()

									wg.Wait()

									if totalBids > 0 && totalAsks > 0 {

										refMap.TotalBidLevels = totalBids
										refMap.TotalAskLevels = totalAsks
										refMap.Spread = refLiveBook.Asks[0].Price - refLiveBook.Bids[0].Price
										refMap.ProcessingTime = time.Since(start)
										refLiveBook.Stats = *refMap
										(*statsMemomory)[composedSymbol] = refMap
										(*liveBookMemomory)[composedSymbol] = refLiveBook

										serialized, err := json.Marshal((*liveBookMemomory)[composedSymbol])
										if err != nil {
											logrus.Error(b.base.Name+" ", err)
											continue
										}

										if os.Getenv("MODE") == "push" {
											if b.base.PushServer {
												if err = b.base.NanoMSG.Send(append([]byte(composedSymbol+"-"+b.base.Name+"-book"), serialized...)); err != nil {
													logrus.Error("Failed publishing: ", err.Error())
												}
											}
										} else {
											bookEncoded := &orderbook.EncodedOrderBook{}
											bookEncoded.CurrencyPair = composedSymbol
											bookEncoded.Exchange = b.base.Name
											bookEncoded.HiveTable = strings.TrimSpace(b.base.Name + "_orderbook_" + composedSymbol)
											bookEncoded.Data, err = compress.Flate(serialized, 3)
											if err != nil {
												logrus.Println(b.base.Name, " ", composedSymbol, "Problem to compress the data to send to kafka")
												continue
											}
											bookEncoded.LastUpdate = b.base.MakeTimestamp()
											serialized, err = json.Marshal(bookEncoded)
											if err != nil {
												logrus.Error(b.base.Name+" ", err)
												continue
											}

											if b.base.KafkaStreaming {
												b.base.KafkaProducer.StreamingMessage(b.base.Name+"_orderbook_"+composedSymbol, string(serialized), b.base.Verbose)
											}
										}
									}

								}
							case "trades":
								if len(chanData) == 3 {
									if chanData[1] == "tu" {
										continue
									}
									data := chanData[2].([]interface{})
									position, exist := b.StringInSlice(chanInfo.Pair, b.base.ExchangeEnabledPairs)
									if exist {
										symbolChannel := strings.Split(b.base.APIEnabledPairs[position], "/")

										composedSymbol := symbolChannel[0] + symbolChannel[1]
										statsMemomory := &b.OrderBookBasicStats
										refMap := (*statsMemomory)[composedSymbol]

										var side string
										var size float64

										if data[2].(float64) < 0 {
											side = "sell"
											size = data[2].(float64) * -1
										} else {
											side = "buy"
											size = data[2].(float64)
										}

										if refMap.LastDealPrice > 0 {
											if refMap.LastDealPrice == data[3].(float64) {
												refMap.TotalTradesBeforePriceImprovement++
												refMap.TotalSizeBeforePriceImprovement = refMap.TotalSizeBeforePriceImprovement + size
												refMap.Improvement = false
											} else {
												refMap.TotalSizeBeforePriceImprovement = size
												refMap.TotalTradesBeforePriceImprovement = 1
												refMap.Improvement = true

												refMap.PercentagePriceImprovement = b.base.PercentChange(refMap.LastDealPrice, data[3].(float64))

												refMap.WaitingTimeTillPriceImprovement = time.Since(time.Unix(0, refMap.LastDealTimestamp)).Nanoseconds()
											}

											refMap.LastDealPrice = data[3].(float64)
											refMap.LastDealAmount = size
											refMap.LastDealSide = side
											refMap.LastDealTimestamp = b.base.MakeTimestamp()

											(*statsMemomory)[composedSymbol] = refMap

											var trades Trade
											trades.Price = float64(data[3].(float64))
											trades.Amount = size
											trades.Side = side
											trades.Timestamp = b.base.MakeTimestamp()
											trades.HiveTable = strings.TrimSpace(b.base.Name + "_trade_" + composedSymbol)

											serialized, err := json.Marshal(trades)
											if err != nil {
												logrus.Error(b.base.Name+" ", err)
												continue
											}

											if os.Getenv("MODE") == "push" {
												if b.base.PushServer {
													if err = b.base.NanoMSG.Send(append([]byte(composedSymbol+"-"+b.base.Name+"-trade"), serialized...)); err != nil {
														logrus.Error("Failed publishing: ", err.Error())
													}
												}
											} else {
												if b.base.KafkaStreaming {
													b.base.KafkaProducer.StreamingMessage(b.base.Name+"_trade_"+composedSymbol, string(serialized), b.base.Verbose)
												}
											}
										} else {
											refMap.LastDealPrice = float64(data[3].(float64))
											refMap.LastDealAmount = size
											refMap.LastDealSide = side
											refMap.LastDealTimestamp = b.base.MakeTimestamp()

											if side == "buy" {
												refMap.SideImprovment = 1
											} else {
												refMap.SideImprovment = 0
											}
											(*statsMemomory)[composedSymbol] = refMap
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}()
}
