package bitfinex

import (
	"errors"
	"log"
	"os"

	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/currency/pair"
	"github.com/maurodelazeri/westeros/exchanges"
	"github.com/maurodelazeri/westeros/exchanges/orderbook"
	"github.com/sirupsen/logrus"
)

// GetExchangePullingType return the type of pulling data the exchange is using
func (b *Bitfinex) GetExchangePullingType() string {
	if b.Websocket {
		return "socket"
	}
	return "rest"
}

// Start starts the Bitfinex go routine
func (b *Bitfinex) Start() {
	go b.Run()
}

// Run implements the Bitfinex wrapper
func (b *Bitfinex) Run() {
	if b.Verbose {
		log.Printf("%s Websocket: %s.", b.GetName(), common.IsEnabled(b.Websocket))
		log.Printf("%s polling delay: %ds.\n", b.GetName(), b.RESTPollingDelay)
		log.Printf("%s %d currencies enabled: %s.\n", b.GetName(), len(b.APIEnabledPairs), b.APIEnabledPairs)
	}

	mode := os.Getenv("MODE")
	if mode == "streaming" || mode == "push" {
		if b.Websocket {

			var dedicatedSocket, sharedSocket []string
			for _, pair := range b.ExchangeEnabledPairs {
				_, exist := b.StringInSlice(pair, b.WebsocketDedicated)
				if exist {
					dedicatedSocket = append(dedicatedSocket, pair)
				} else {
					sharedSocket = append(sharedSocket, pair)
				}
			}

			if len(dedicatedSocket) > 0 {
				for _, pair := range dedicatedSocket {
					socket := new(WebsocketBitfinex)
					socket.base = b
					socket.url = "wss://api.bitfinex.com/ws/2"
					socket.subscribedPairs = append(socket.subscribedPairs, pair)
					go socket.WebsocketClient()
				}
			}

			if len(sharedSocket) > 0 {
				socket := new(WebsocketBitfinex)
				socket.base = b
				socket.url = "wss://api.bitfinex.com/ws/2"
				socket.subscribedPairs = sharedSocket
				go socket.WebsocketClient()
			}

		}
	} else {
		logrus.Warn("Please specify a mode to start an exchange.")
		os.Exit(1)
	}

}

// UpdateTicker updates and returns the ticker for a currency pair
func (b *Bitfinex) UpdateTicker() {

}

// UpdateOrderbook updates and returns the orderbook for a currency pair
func (b *Bitfinex) UpdateOrderbook(normalized, currency string) (orderbook.Base, error) {
	var orderBook orderbook.Base
	return orderBook, nil
}

// GetExchangeAccountInfo retrieves balances for all enabled currencies on the
// Bitfinex exchange
func (b *Bitfinex) GetExchangeAccountInfo() (exchange.AccountInfo, error) {
	var response exchange.AccountInfo
	response.ExchangeName = b.GetName()
	accountBalance, err := b.GetAccountBalance()
	if err != nil {
		return response, err
	}
	if !b.Enabled {
		return response, nil
	}

	type bfxCoins struct {
		OnHold    float64
		Available float64
	}

	accounts := make(map[string]bfxCoins)

	for i := range accountBalance {
		onHold := accountBalance[i].Amount - accountBalance[i].Available
		coins := bfxCoins{
			OnHold:    onHold,
			Available: accountBalance[i].Available,
		}
		result, ok := accounts[accountBalance[i].Currency]
		if !ok {
			accounts[accountBalance[i].Currency] = coins
		} else {
			result.Available += accountBalance[i].Available
			result.OnHold += onHold
			accounts[accountBalance[i].Currency] = result
		}
	}

	for x, y := range accounts {
		var exchangeCurrency exchange.AccountCurrencyInfo
		exchangeCurrency.CurrencyName = common.StringToUpper(x)
		exchangeCurrency.TotalValue = y.Available + y.OnHold
		exchangeCurrency.Hold = y.OnHold
		response.Currencies = append(response.Currencies, exchangeCurrency)
	}

	return response, nil
}

// GetExchangeHistory returns historic trade data since exchange opening.
func (b *Bitfinex) GetExchangeHistory(p pair.CurrencyPair, assetType string) ([]exchange.TradeHistory, error) {
	var resp []exchange.TradeHistory

	return resp, errors.New("trade history not yet implemented")
}
