package bitmex

import (
	"errors"
	"log"
	"os"

	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/currency/pair"
	exchange "github.com/maurodelazeri/westeros/exchanges"
	"github.com/maurodelazeri/westeros/exchanges/orderbook"
	"github.com/sirupsen/logrus"
)

// GetExchangePullingType return the type of pulling data the exchange is using
func (b *Bitmex) GetExchangePullingType() string {
	if b.Websocket {
		return "socket"
	}
	return "rest"
}

// Start starts the bitmex go routine
func (b *Bitmex) Start() {
	go b.Run()
}

// Run implements the bitmex wrapper
func (b *Bitmex) Run() {
	if b.Verbose {
		log.Printf("%s Websocket: %s. (url: %s).\n", b.GetName(), common.IsEnabled(b.Websocket), bitmexWebsocketURL)
		log.Printf("%s polling delay: %ds.\n", b.GetName(), b.RESTPollingDelay)
		log.Printf("%s %d currencies enabled: %s.\n", b.GetName(), len(b.APIEnabledPairs), b.APIEnabledPairs)
	}

	mode := os.Getenv("MODE")
	if mode == "streaming" || mode == "push" {
		if b.Websocket {
			var dedicatedSocket, sharedSocket []string
			for _, pair := range b.ExchangeEnabledPairs {
				_, exist := b.StringInSlice(pair, b.WebsocketDedicated)
				if exist {
					dedicatedSocket = append(dedicatedSocket, pair)
				} else {
					sharedSocket = append(sharedSocket, pair)
				}
			}

			if len(dedicatedSocket) > 0 {
				for _, pair := range dedicatedSocket {
					socket := new(WebsocketBitmex)
					socket.base = b
					socket.subscribedPairs = append(socket.subscribedPairs, pair)
					go socket.WebsocketClient()
				}
			}

			if len(sharedSocket) > 0 {
				socket := new(WebsocketBitmex)
				socket.base = b
				socket.subscribedPairs = sharedSocket
				go socket.WebsocketClient()
			}
		}
	} else {
		logrus.Warn("Please specify a mode to start an exchange.")
		os.Exit(1)
	}
}

// GetExchangeAccountInfo retrieves balances for all enabled currencies for the
// bitmex exchange
func (b *Bitmex) GetExchangeAccountInfo() (exchange.AccountInfo, error) {
	var response exchange.AccountInfo
	return response, nil
}

// UpdateTicker updates and returns the ticker for a currency pair
func (b *Bitmex) UpdateTicker() {

}

// UpdateOrderbook updates and returns the orderbook for a currency pair
func (b *Bitmex) UpdateOrderbook(normalized, currency string) (orderbook.Base, error) {
	var orderBook orderbook.Base
	return orderBook, nil
}

// GetExchangeHistory returns historic trade data since exchange opening.
func (b *Bitmex) GetExchangeHistory(p pair.CurrencyPair, assetType string) ([]exchange.TradeHistory, error) {
	var resp []exchange.TradeHistory

	return resp, errors.New("trade history not yet implemented")
}
