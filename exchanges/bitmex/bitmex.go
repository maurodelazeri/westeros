package bitmex

import (
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/maurodelazeri/westeros/kafka"
	"github.com/maurodelazeri/westeros/push"

	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/config"
	"github.com/maurodelazeri/westeros/exchanges"
	"github.com/maurodelazeri/westeros/exchanges/orderbook"
	"github.com/maurodelazeri/westeros/exchanges/request"
	"github.com/maurodelazeri/westeros/exchanges/ticker"
)

const (
	bitmexAPIURL = "https://api.bitmex.com/"

	bitmexAuthRate   = 0
	bitmexUnauthRate = 0
)

var sometin []string

// Bitmex is the overarching type across the bitmex package
type Bitmex struct {
	exchange.Base
	*request.Handler
}

// WebsocketBitmex is the overarching type across the bitmex package
type WebsocketBitmex struct {
	exchange.Base
	base            *Bitmex
	subscribedPairs []string

	*request.Handler

	nonce       int64
	isConnected bool
	mu          sync.Mutex
	*websocket.Conn
	dialer    *websocket.Dialer
	url       string
	reqHeader http.Header
	httpResp  *http.Response
	dialErr   error

	// RecIntvlMin specifies the initial reconnecting interval,
	// default to 2 seconds
	RecIntvlMin time.Duration
	// RecIntvlMax specifies the maximum reconnecting interval,
	// default to 30 seconds
	RecIntvlMax time.Duration
	// RecIntvlFactor specifies the rate of increase of the reconnection
	// interval, default to 1.5
	RecIntvlFactor float64
	// HandshakeTimeout specifies the duration for the handshake to complete,
	// default to 2 seconds
	HandshakeTimeout time.Duration

	OrderBookMAP        map[string]map[int64][]float64
	LiveOrderBook       map[string]*orderbook.Base
	OrderBookBasicStats map[string]*orderbook.BookStats

	OrderBookStart map[string]bool
}

// SetDefaults sets default values for the exchange
func (b *Bitmex) SetDefaults() {
	b.Name = "bitmex"
	b.Enabled = false
	b.Verbose = false
	b.Websocket = false
	b.RESTPollingDelay = 10
	b.AssetTypes = []string{ticker.Spot}
	b.APIUrl = bitmexAPIURL
	b.Handler = new(request.Handler)
	b.SetRequestHandler(b.Name, bitmexAuthRate, bitmexUnauthRate, new(http.Client))
}

// Setup initialises the exchange parameters with the current configuration
func (b *Bitmex) Setup(exch config.ExchangeConfig) {
	if !exch.Enabled {
		b.SetEnabled(false)
	} else {
		b.Enabled = true
		b.AuthenticatedAPISupport = exch.AuthenticatedAPISupport
		b.SetAPIKeys(exch.APIKey, exch.APISecret, exch.ClientID, true)
		b.RESTPollingDelay = exch.RESTPollingDelay
		b.Verbose = exch.Verbose
		b.Websocket = exch.Websocket
		b.BaseCurrencies = common.SplitStrings(exch.BaseCurrencies, ",")
		b.APIEnabledPairs = exch.APIEnabledPairs
		b.ExchangeEnabledPairs = exch.ExchangeEnabledPairs
		b.MakerFee = exch.MakerFee
		b.TakerFee = exch.TakerFee
		b.KafkaStreaming = exch.KafkaStreaming
		b.PushServer = exch.PushServer
		b.WebsocketDedicated = exch.WebsocketDedicated

		// b.RedisServer = new(redisserver.RedisServer)
		// b.RedisServer.Initialize()

		if os.Getenv("MODE") == "push" {
			if b.PushServer {
				push := new(nannomsg.NanomsgServer)
				push.Initialize(b.Name)
				b.NanoMSG = push.NanoMSG
			}
		} else {
			if b.KafkaStreaming {
				b.KafkaProducer = new(kafkaserver.KafkaProducer)
				b.KafkaProducer.Initialize()
			}
		}

	}
}
