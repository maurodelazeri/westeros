package bitmex

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/jpillora/backoff"
	"github.com/maurodelazeri/westeros/common"
	"github.com/maurodelazeri/westeros/compress"
	"github.com/maurodelazeri/westeros/exchanges/orderbook"
	"github.com/sirupsen/logrus"
)

const (
	bitmexWebsocketURL = "wss://www.bitmex.com/realtime"
)

// WebsocketSubscribe subsribe public and private endpoints
func (b *WebsocketBitmex) WebsocketSubscribe(endpoint []string) error {
	subscribe := WebsocketSubscription{"subscribe", endpoint}
	json, err := common.JSONEncode(subscribe)
	if err != nil {
		return err
	}
	err = b.Conn.WriteMessage(websocket.TextMessage, json)
	if err != nil {
		return err
	}
	return nil
}

// Close closes the underlying network connection without
// sending or waiting for a close frame.
func (b *WebsocketBitmex) Close() {
	b.mu.Lock()
	if b.Conn != nil {
		b.Conn.Close()
	}
	b.isConnected = false
	b.mu.Unlock()
}

func (b *WebsocketBitmex) Loop() {
	b.WebsocketClient()
}

func (b *WebsocketBitmex) connect() {

	bb := &backoff.Backoff{
		Min:    b.RecIntvlMin,
		Max:    b.RecIntvlMax,
		Factor: b.RecIntvlFactor,
		Jitter: true,
	}

	rand.Seed(time.Now().UTC().UnixNano())

	b.OrderBookStart = make(map[string]bool)

	b.OrderBookMAP = make(map[string]map[int64][]float64)
	b.OrderBookBasicStats = make(map[string]*orderbook.BookStats)
	b.LiveOrderBook = make(map[string]*orderbook.Base)

	for _, sym := range b.subscribedPairs {

		position, exist := b.base.StringInSlice(sym, b.base.ExchangeEnabledPairs)
		symbol := strings.Split(b.base.APIEnabledPairs[position], "/")
		composedSymbol := symbol[0] + symbol[1]
		if exist {

			b.OrderBookBasicStats[composedSymbol] = &orderbook.BookStats{}

			b.OrderBookMAP[composedSymbol+"bids"] = make(map[int64][]float64)
			b.OrderBookMAP[composedSymbol+"asks"] = make(map[int64][]float64)

			// err := b.base.RedisServer.RedisClient.Del("stats" + composedSymbol + b.base.Name).Err()
			// if err != nil {
			// 	logrus.Error(err)
			// }
		}
	}

	for {
		nextItvl := bb.Duration()

		wsConn, httpResp, err := b.dialer.Dial(bitmexWebsocketURL, b.reqHeader)

		b.mu.Lock()
		b.Conn = wsConn
		b.dialErr = err
		b.isConnected = err == nil
		b.httpResp = httpResp
		b.mu.Unlock()

		if err == nil {
			if b.base.Verbose {
				log.Printf("Dial: connection was successfully established with %s\n", bitmexWebsocketURL)
			}

			// Subscribe trade
			endpoint := []string{}
			for _, x := range b.subscribedPairs {
				endpoint = append(endpoint, "trade:"+x)
				endpoint = append(endpoint, "orderBookL2:"+x)
				// endpoint = append(endpoint, "instrument:"+x)
				// endpoint = append(endpoint, "insurance:"+x)
				// endpoint = append(endpoint, "settlement:"+x)
			}
			err = b.WebsocketSubscribe(endpoint)
			if err != nil {
				log.Printf("Websocket send data error:%v", err)
			}

			break
		} else {
			if b.base.Verbose {
				log.Println(err)
				log.Println("Dial: will try again in", nextItvl, "seconds.")
			}
		}

		time.Sleep(nextItvl)
	}
}

func (b *WebsocketBitmex) WebsocketClient() {

	if b.RecIntvlMin == 0 {
		b.RecIntvlMin = 2 * time.Second
	}

	if b.RecIntvlMax == 0 {
		b.RecIntvlMax = 30 * time.Second
	}

	if b.RecIntvlFactor == 0 {
		b.RecIntvlFactor = 1.5
	}

	if b.HandshakeTimeout == 0 {
		b.HandshakeTimeout = 2 * time.Second
	}

	b.dialer = websocket.DefaultDialer
	b.dialer.HandshakeTimeout = b.HandshakeTimeout

	// Start reading from the socket
	b.startReading()

	go func() {
		b.connect()
	}()

	// wait on first attempt
	time.Sleep(b.HandshakeTimeout)

}

// IsConnected returns the WebSocket connection state
func (b *WebsocketBitmex) IsConnected() bool {
	b.mu.Lock()
	defer b.mu.Unlock()

	return b.isConnected
}

// CloseAndRecconect will try to reconnect.
func (b *WebsocketBitmex) closeAndRecconect() {
	b.Close()
	go func() {
		b.connect()
	}()
}

// GetHTTPResponse returns the http response from the handshake.
// Useful when WebSocket handshake fails,
// so that callers can handle redirects, authentication, etc.
func (b *WebsocketBitmex) GetHTTPResponse() *http.Response {
	b.mu.Lock()
	defer b.mu.Unlock()

	return b.httpResp
}

// GetDialError returns the last dialer error.
// nil on successful connection.
func (b *WebsocketBitmex) GetDialError() error {
	b.mu.Lock()
	defer b.mu.Unlock()

	return b.dialErr
}

// ReadMessage is a helper method for getting a reader
// using NextReader and reading from that reader to a buffer.
//
// If the connection is closed ErrNotConnected is returned
// ReadMessage initiates a websocket client
func (b *WebsocketBitmex) startReading() {
	go func() {
		for {
			select {
			default:
				if b.IsConnected() {
					err := ErrNotConnected
					msgType, resp, err := b.Conn.ReadMessage()
					if err != nil {
						logrus.Error(b.base.Name, " problem to read: ", err)
						b.closeAndRecconect()
						continue
					}
					switch msgType {
					case websocket.TextMessage:
						data := wsData{}
						err := common.JSONDecode(resp, &data)
						if err != nil {
							log.Println(err)
							continue
						}
						var table wsData
						json.Unmarshal(resp, &table)

						switch table.Table {
						case "orderBookL2":
							var book []OrderBookL2
							err := json.Unmarshal(table.Data, &book)
							if err != nil {
								logrus.Error(b.base.Name+" ", err)
								continue
							}
							if data.Action == "partial" {
								b.OrderBookStart[book[0].Symbol] = true
							}
							if !b.OrderBookStart[book[0].Symbol] {
								continue
							}

							start := time.Now()
							if len(book) > 0 {
								position, exist := b.StringInSlice(book[0].Symbol, b.base.ExchangeEnabledPairs)
								if exist {
									symbolChannel := strings.Split(b.base.APIEnabledPairs[position], "/")

									composedSymbol := symbolChannel[0] + symbolChannel[1]
									statsMemomory := &b.OrderBookBasicStats
									refMap := (*statsMemomory)[composedSymbol]

									liveBookMemomory := &b.LiveOrderBook
									refLiveBook := (*liveBookMemomory)[composedSymbol]
									refLiveBook = &orderbook.Base{}
									refLiveBook.LastUpdate = b.base.MakeTimestamp()
									refLiveBook.WeekDay = int(time.Now().Weekday())
									refLiveBook.CurrencyPair = composedSymbol
									refLiveBook.Exchange = b.base.Name

									for _, one := range book {
										switch data.Action {
										case "insert":
											if one.Side == "Buy" {
												b.OrderBookMAP[composedSymbol+"bids"][one.ID] = []float64{one.Price, one.Size, float64(one.ID)}
											} else {
												b.OrderBookMAP[composedSymbol+"asks"][one.ID] = []float64{one.Price, one.Size, float64(one.ID)}
											}
										case "partial":
											if one.Side == "Buy" {
												refLiveBook.Bids = append(refLiveBook.Bids, orderbook.Item{Price: one.Price, Amount: one.Size, ID: strconv.FormatInt(one.ID, 10)})
											} else {
												refLiveBook.Asks = append(refLiveBook.Asks, orderbook.Item{Price: one.Price, Amount: one.Size, ID: strconv.FormatInt(one.ID, 10)})
											}
										case "update":
											if one.Side == "Buy" {
												if _, ok := b.OrderBookMAP[composedSymbol+"bids"][one.ID]; ok {
													b.OrderBookMAP[composedSymbol+"bids"][one.ID] = []float64{b.OrderBookMAP[composedSymbol+"bids"][one.ID][0], one.Size}
												}
											} else {
												if _, ok := b.OrderBookMAP[composedSymbol+"asks"][one.ID]; ok {
													b.OrderBookMAP[composedSymbol+"asks"][one.ID] = []float64{b.OrderBookMAP[composedSymbol+"asks"][one.ID][0], one.Size}
												}
											}
										case "delete":
											if one.Side == "Buy" {
												if _, ok := b.OrderBookMAP[composedSymbol+"bids"][one.ID]; ok {
													delete(b.OrderBookMAP[composedSymbol+"bids"], one.ID)
												}
											} else {
												if _, ok := b.OrderBookMAP[composedSymbol+"asks"][one.ID]; ok {
													delete(b.OrderBookMAP[composedSymbol+"asks"], one.ID)
												}
											}
										}
									}

									// removing the noize levels, keeping 50 asks 50 bids
									if data.Action == "partial" {
										var wg sync.WaitGroup
										wg.Add(1)
										go func() {
											sort.Sort(refLiveBook.Bids)
											for _, values := range refLiveBook.Bids {
												id, _ := strconv.ParseInt(values.ID, 10, 64)
												b.OrderBookMAP[composedSymbol+"bids"][id] = []float64{values.Price, values.Amount}
											}
											wg.Done()
										}()
										wg.Add(1)
										go func() {
											sort.Sort(refLiveBook.Asks)
											for _, values := range refLiveBook.Asks {
												id, _ := strconv.ParseInt(values.ID, 10, 64)
												b.OrderBookMAP[composedSymbol+"asks"][id] = []float64{values.Price, values.Amount}
											}
											wg.Done()
										}()
										wg.Wait()
										refLiveBook.Stats = *refMap
										(*statsMemomory)[composedSymbol] = refMap
										(*liveBookMemomory)[composedSymbol] = refLiveBook
										continue
									}

									var wg sync.WaitGroup

									wg.Add(1)
									go func() {
										for _, values := range b.OrderBookMAP[composedSymbol+"bids"] {
											refLiveBook.Bids = append(refLiveBook.Bids, orderbook.Item{Price: values[0], Amount: values[1]})
										}
										sort.Sort(refLiveBook.Bids)
										wg.Done()
									}()
									wg.Add(1)
									go func() {
										for _, values := range b.OrderBookMAP[composedSymbol+"asks"] {
											refLiveBook.Asks = append(refLiveBook.Asks, orderbook.Item{Price: values[0], Amount: values[1]})
										}
										sort.Sort(refLiveBook.Asks)
										wg.Done()
									}()

									wg.Wait()

									var totalBids, totalAsks int

									wg.Add(1)
									go func() {
										totalBids = len(refLiveBook.Bids)
										if totalBids > 20 {
											refLiveBook.Bids = refLiveBook.Bids[0:20]
											totalBids = 20
										}
										wg.Done()
									}()
									wg.Add(1)
									go func() {
										totalAsks = len(refLiveBook.Asks)
										if totalAsks > 20 {
											refLiveBook.Asks = refLiveBook.Asks[0:20]
											totalAsks = 20
										}
										wg.Done()
									}()

									wg.Wait()

									if totalBids > 0 && totalAsks > 0 {

										refMap.TotalBidLevels = totalBids
										refMap.TotalAskLevels = totalAsks
										refMap.Spread = refLiveBook.Asks[0].Price - refLiveBook.Bids[0].Price
										refMap.ProcessingTime = time.Since(start)
										refLiveBook.Stats = *refMap
										(*statsMemomory)[composedSymbol] = refMap
										(*liveBookMemomory)[composedSymbol] = refLiveBook

										serialized, err := json.Marshal((*liveBookMemomory)[composedSymbol])
										if err != nil {
											logrus.Error(b.base.Name+" ", err)
											continue
										}

										if os.Getenv("MODE") == "push" {
											if b.base.PushServer {
												if err = b.base.NanoMSG.Send(append([]byte(composedSymbol+"-"+b.base.Name+"-book"), serialized...)); err != nil {
													logrus.Error("Failed publishing: ", err.Error())
												}
											}
										} else {
											bookEncoded := &orderbook.EncodedOrderBook{}
											bookEncoded.CurrencyPair = composedSymbol
											bookEncoded.Exchange = b.base.Name
											bookEncoded.HiveTable = strings.TrimSpace(b.base.Name + "_orderbook_" + composedSymbol)
											bookEncoded.Data, err = compress.Flate(serialized, 3)
											if err != nil {
												logrus.Println(b.base.Name, " ", composedSymbol, "Problem to compress the data to send to kafka")
												continue
											}
											bookEncoded.LastUpdate = b.base.MakeTimestamp()
											serialized, err = json.Marshal(bookEncoded)
											if err != nil {
												logrus.Error(b.base.Name+" ", err)
												continue
											}

											if b.base.KafkaStreaming {
												b.base.KafkaProducer.StreamingMessage(b.base.Name+"_orderbook_"+composedSymbol, string(serialized), b.base.Verbose)
											}
										}

									}
								}
							}
						case "trade":
							var trades []WSTrade
							err := json.Unmarshal(table.Data, &trades)
							if err != nil {
								logrus.Error(b.base.Name+" ", err)
								continue
							}
							for _, one := range trades {

								position, exist := b.StringInSlice(trades[0].Symbol, b.base.ExchangeEnabledPairs)
								if exist {
									symbolChannel := strings.Split(b.base.APIEnabledPairs[position], "/")

									composedSymbol := symbolChannel[0] + symbolChannel[1]
									statsMemomory := &b.OrderBookBasicStats
									refMap := (*statsMemomory)[composedSymbol]

									if refMap.LastDealPrice > 0 {
										if refMap.LastDealPrice == one.Price {
											refMap.TotalTradesBeforePriceImprovement++
											refMap.TotalSizeBeforePriceImprovement = refMap.TotalSizeBeforePriceImprovement + one.Size
											refMap.Improvement = false
										} else {
											refMap.TotalSizeBeforePriceImprovement = one.Size
											refMap.TotalTradesBeforePriceImprovement = 1
											refMap.Improvement = true

											refMap.PercentagePriceImprovement = b.base.PercentChange(refMap.LastDealPrice, one.Price)

											refMap.WaitingTimeTillPriceImprovement = time.Since(time.Unix(0, refMap.LastDealTimestamp)).Nanoseconds()
										}

										refMap.LastDealPrice = one.Price
										refMap.LastDealAmount = one.Size
										refMap.LastDealSide = one.Side
										refMap.LastDealTimestamp = b.base.MakeTimestamp()

										(*statsMemomory)[composedSymbol] = refMap

										obj := &one
										obj.HiveTable = strings.TrimSpace(b.base.Name + "_trade_" + composedSymbol)

										serialized, err := json.Marshal(&one)
										if err != nil {
											logrus.Error(b.base.Name+" ", err)
											continue
										}

										if os.Getenv("MODE") == "push" {
											if b.base.PushServer {
												if err = b.base.NanoMSG.Send(append([]byte(composedSymbol+"-"+b.base.Name+"-trade"), serialized...)); err != nil {
													logrus.Error("Failed publishing: ", err.Error())
												}
											}
										} else {
											if b.base.KafkaStreaming {
												b.base.KafkaProducer.StreamingMessage(b.base.Name+"_trade_"+composedSymbol, string(serialized), b.base.Verbose)
											}
										}
									} else {
										refMap.LastDealPrice = one.Price
										refMap.LastDealAmount = one.Size
										refMap.LastDealSide = one.Side
										refMap.LastDealTimestamp = b.base.MakeTimestamp()

										if one.Side == "Buy" {
											refMap.SideImprovment = 1
										} else {
											refMap.SideImprovment = 0
										}

										(*statsMemomory)[composedSymbol] = refMap
									}
								}
							}
							// case "instrument":
							// 	serialized, _ := json.Marshal(data)
							// 	if b.base.KafkaStreaming {
							// 		b.base.KafkaProducer.StreamingMessage(b.base.Name+"_full_"+composedSymbol, string(serialized), b.base.Verbose)
							// 	}
							// case "insurance":
							// 	serialized, _ := json.Marshal(data)
							// 	if b.base.KafkaStreaming {
							// 		b.base.KafkaProducer.StreamingMessage(b.base.Name+"_full_"+composedSymbol, string(serialized), b.base.Verbose)
							// 	}
							// case "settlement":
							// 	serialized, _ := json.Marshal(data)
							// 	if b.base.KafkaStreaming {
							// 		b.base.KafkaProducer.StreamingMessage(b.base.Name+"_full_"+composedSymbol, string(serialized), b.base.Verbose)
							// 	}
						}
					}
				}
			}
		}
	}()
}
