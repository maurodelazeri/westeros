package nannomsg

import (
	"os"

	"github.com/sirupsen/logrus"
	mangos "nanomsg.org/go-mangos"
	"nanomsg.org/go-mangos/protocol/pub"
	"nanomsg.org/go-mangos/transport/tcp"
)

// NanomsgServer stores the sessiion
type NanomsgServer struct {
	NanoMSG mangos.Socket
}

// Initialize nanomsg instance
func (p *NanomsgServer) Initialize(exchange string) {
	var url string
	switch exchange {
	case "gdax":
		url = os.Getenv("PUSH_GDAX_BIND")
	case "binance":
		url = os.Getenv("PUSH_BINANCE_BIND")
	case "bitfinex":
		url = os.Getenv("PUSH_BITFINEX_BIND")
	case "bitmex":
		url = os.Getenv("PUSH_BITMEX_BIND")
	default:
		logrus.Warn("Nanomsg cant initialized, exchange does not exist: ", exchange)
		os.Exit(1)
	}
	socket, err := pub.NewSocket()
	if err != nil {
		logrus.Error("Problem to initialize NanoMSG: ", err)
		os.Exit(1)
	}
	//	p.NanoMSG.AddTransport(ipc.NewTransport())
	socket.AddTransport(tcp.NewTransport())
	err = socket.Listen(url)
	p.NanoMSG = socket

	if err != nil {
		logrus.Error("Problem bind NanoMSG: ", err)
		os.Exit(1)
	}
}
