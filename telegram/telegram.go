package telegram

import (
	"log"
	"os"
	"strconv"

	"gopkg.in/telegram-bot-api.v4"
)

// SendMessage sends a message to telegram
func SendMessage(message string) {
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAMAPI"))
	if err != nil {
		log.Panic(err)
	}

	group, _ := strconv.ParseInt(os.Getenv("TELEGRAMGROUP"), 10, 64)
	msg := tgbotapi.NewMessage(group, message)

	bot.Send(msg)
}
